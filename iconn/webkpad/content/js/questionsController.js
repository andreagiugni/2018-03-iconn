var stringaRisposte="";
var risposte=new Array();

risposte.deleteElem = function ( val ) {
    for ( var i = 0; i < this.length; i++ ) {
        if ( this[i].toString() == val.toString() ) {
            this.splice( i, 1 );
            return i;
        }
    }
};


risposte.setSingleItem = function ( val ) {
	this.length=0;
	this.push(val);
};

function showQuestion(q,count){
	
			
				ajax({
					method:"GET",
					url:"../getQuestion.php",
					async:true,
					data: "q="+q+"&count="+count
					}).done(function(response){
						
						var questionGroup=response.split("###");
						var htmlToInsert="";
						max_anwer=questionGroup[1];
						htmlToInsert+='<div norder="'+questionGroup[2]+'" id="questionBox">'+labelCentered(questionGroup[4])+' </div>'; //'+labelCentered(questionGroup[4])+' 
					//	for(var j=0;j<3;j++)  // ****DA TOGLIERE, SERVE SOLO PER MOLTIPLICARE LE OPZIONI DURANTE I TEST
						for(var i=5;i<questionGroup.length-1;i+=2)
							htmlToInsert+='<div class="answerBox" name="answer" id="answerBox'+questionGroup[i]+'" onClick="segna_risposta('+max_anwer+','+questionGroup[i]+')">'+questionGroup[i+1]+'</div>';
						
						
						htmlToInsert+='<div id="btnSend" onClick="sendAnswer('+q+','+count+')" style="display:none">send</div>';  
						document.getElementById("questionContainer").innerHTML=htmlToInsert;
					//	document.getElementById("notAquestion").style.display='none';
						
					//	document.getElementById("questionContainer").style.display='block';
					
				});			
	
}

function segna_risposta(max_anwer,answerNumber){
	var el=document.getElementById("answerBox"+answerNumber);
	if(hasClass(el,"selected")){
			removeClass(el,"selected");
			risposte.deleteElem(el.id.replace("answerBox","")); //elimino la parola 'answerBox' dall'id
	}
	else{
		if(max_anwer==1){
			var answerElements=document.getElementsByName("answer");
			for(var i=0;i<answerElements.length;i++)
					removeClass(answerElements[i],"selected");
					risposte.setSingleItem(""+answerNumber);
					document.getElementById("answerBox"+answerNumber).className += ' selected';
		}else{
			if(max_anwer<=risposte.length)
				alert("You can give at most "+max_anwer+" answers");
			else{
				risposte.push(answerNumber);
				document.getElementById("answerBox"+answerNumber).className += ' selected';
				}	
		}
	}
	
	if(risposte.length>0)
		document.getElementById("btnSend").style.display="block";
	else
		document.getElementById("btnSend").style.display="none";	
}

	
function sendAnswer(idQuestion,idVoteHistory) {
	var stringaRisposte="";
	for(var i=0;i<risposte.length;i++){
		stringaRisposte+=risposte[i];
		if(i<risposte.length-1)
			stringaRisposte+=",";
	}
	if (stringaRisposte != "") {
		/* ajax({
			method: "POST",
			url:"../saveAnswer.php",
			data: "pkidquestion=" + idQuestion+ "&pkidroom=" + _ROOM_ + "&pkiddevice=" + _DEVICE_ + "&pkidanswer=" + stringaRisposte + "&pkidvotehistory=" + idVoteHistory,
			async: true
		}).done(function(status) { 
			if(status.indexOf("OK")!=-1){
				addQuestionAnswered(idQuestion,idVoteHistory);
				document.getElementById("questionContainer").innerHTML='<div id="thankYou" >'+labelCentered("THANK YOU!")+'</div>';
				setTimeout(function(){
					ANSWERING = false;
					//flush();
				},3000);
			}
			else{
				alert("An error occurred, try again");
			}
		});*/
		/***** MODIFICA FATTA IN CORSA PER EVITARE CHE MANDINO PIù VOLTE LA STESSA RISPOSTA*****/
		addQuestionAnswered(idQuestion,idVoteHistory);
		document.getElementById("questionContainer").innerHTML='<div id="thankYou" >'+labelCentered("THANK YOU!")+'</div>';
		setTimeout(function(){
			ANSWERING = false;
			//flush();
		},3000); 
		//alert();
		ajax({
			method: "POST",
			url:"../saveAnswer.php",
			data: "pkidquestion=" + idQuestion+ "&pkidroom=" + _ROOM_ + "&pkiddevice=" + _DEVICE_ + "&pkidanswer=" + stringaRisposte + "&pkidvotehistory=" + idVoteHistory,
			async: true
		});
	}
}
	
	
function flush(){
	document.getElementById("questionContainer").innerHTML='';
	//document.getElementById("questionContainer").style.display='none';
	//document.getElementById("notAquestion").style.display='block';
	
}			
	
function hasClass(ele,cls) {
    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

function removeClass(ele,cls) {
	if (hasClass(ele,cls)) {
		var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		ele.className=ele.className.replace(reg,' ');
	}
}	

	
	
var x,y,n=0,ny=0,rotINT,rotYINT

function showThanks()
{
	/*y=document.getElementById("questionContainer")
	clearInterval(rotYINT)
	rotYINT=setInterval(function(){
		ny=ny+1;
		y.style.transform="rotateY(" + ny + "deg)";
		y.style.webkitTransform="rotateY(" + ny + "deg)";
		y.style.OTransform="rotateY(" + ny + "deg)";
		y.style.MozTransform="rotateY(" + ny + "deg)";
		if (ny==90 ){
			ny=ny-180;
			document.getElementById("questionContainer").innerHTML='<div id="questionBox" >'+labelCentered("THANK YOU!")+'</div>';
		}
		if (ny==0)
			clearInterval(rotYINT);
	},2);*/
	clearInterval(INTERVAL);
	document.getElementById("questionContainer").innerHTML='<div id="questionBox" >'+labelCentered("THANK YOU!")+'</div>';
}


function addQuestionAnswered(question_id,voting_session){
	var name=question_id+"_"+voting_session;
	localStorage.setItem(name, 1);
}

function isAnswered(question_id,voting_session){
	var name=question_id+"_"+voting_session;
	var tmp=localStorage.getItem(name);
	if(tmp) return true;
	else return false;
}

function labelCentered(lbl){
	return '<table width="100%" height="100%"><tr><td align=center>'+lbl+'</td></tr></table>';
}