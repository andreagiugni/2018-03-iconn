<?php 
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Symfony\Routing\RouteCollection;
use MyApp\Chat;
error_reporting(E_ALL);
ini_set('display_errors', 1);
    require dirname(__DIR__) . '/vendor/autoload.php';

	
	$collection = new RouteCollection;
    $collection->add('chat', new Route('/chat', array(
        '_controller'    => new Chat
      , 'allowedOrigins' => 'socketo.me'
    )));
    $collection->add('echo', new Route('/echo', array(
        '_controller'    => new Ratchet\Server\EchoServer
      , 'allowedOrigins' => '*'
    )));
	/*
    $server = IoServer::factory(
        new HttpServer(
            new WsServer(
                new Chat()
            )
        ),
        8080
    );
	*/
	$router = new WsRouter;
    $router->addCollection($collection);
    $wsserv = new WebSocket($router);
    $server = new IoServer($wsserv);
    $server->run();

	