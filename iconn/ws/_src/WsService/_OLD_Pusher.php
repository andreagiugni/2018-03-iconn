<?php
namespace MyApp;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;  
use Ratchet\Wamp\WampServerInterface;
use React\EventLoop\LoopInterface;
error_reporting(E_ALL);
ini_set('display_errors', 1);
	
class Pusher implements MessageComponentInterface,WampServerInterface
{
   protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
		$conn->send('Welcome on board');
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }
    }
	
	public function onServiceCall($entry) {
        $entryData = json_decode($entry, true);
		$service="questions";
		$service->broadcast($entryData);
		
		/*foreach ($this->clients as $client) {
            
                // The sender is not the receiver, send to each client connected
                $client->send($entry);
            
        }
*/
        // If the lookup topic object isn't set there is no one to publish to
        /*if (!array_key_exists($entryData['service'], $this->subscribedServices)) {
            return;
        }

        $topic = $this->subscribedServices[$entryData['service']];

        // re-send the data to all the clients subscribed to that service
        $topic->broadcast($entryData);*/
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
	
}