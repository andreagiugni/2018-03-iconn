<style>

body {

	font-size:9px;
	font-family:arial;
}

</style>

<body>

<?php

include("connect.php");

function ShowQuestion($i) {
	
global $db;

	$q = "SELECT iAnswer, count(iAnswer) as c FROM tblVirtualPatientAction WHERE fkidPatient = '2' AND iTest = '".$i."' GROUP BY iAnswer";
	
	//echo $q."<br>";
	
	echo "<h2>".LookupQuestions($i)."</h2>";
	
	$rows = $db->GetAll($q);	
	
	foreach($rows as $row) {
		
		echo "<h3>".$row['c']." - ".LookupAnswers($row['iAnswer'])."</h3>";		
	}
	
	echo "<br>";
	
}

echo "<h1>HELEN VIRTUAL PATIENT</h1>";

echo "<h1>Survey 1</h1>";

ShowQuestion(1);

echo "<h1>Survey 2</h1>";

ShowQuestion(2);

echo "<h1>Survey 3</h1>";

ShowQuestion(3);

echo "<h1>Survey 4</h1>";

ShowQuestion(4);

echo "<h1>Survey 5</h1>";

ShowQuestion(5);

echo "<h1>Survey 6</h1>";

ShowQuestion(6);

echo "<h1>Quiz 1</h1>";

ShowQuestion(7);

echo "<h1>Quiz 2</h1>";

ShowQuestion(8);
ShowQuestion(9);


echo "<h1>Quiz 3</h1>";

ShowQuestion(10);
ShowQuestion(11);


/*
echo "<h1>Quiz</h1>";

ShowQuestion(1);
ShowQuestion(2);
ShowQuestion(3);
ShowQuestion(4);
ShowQuestion(5);
ShowQuestion(6);
*/
function LookupQuestions($i) {

switch($i) {
		
	case 1: return "Do you order ANA tests before and during infliximab treatment?";
			break;
	
	case 2: return "In your opinion, is there sufficient evidence to support a recommendation of balneotherapy for a patient with PASI >10?";
			break;
	
	case 3: return "When re-starting infliximab after a treatment interruption, do you:";
			break;
	
	case 4: return "What course of psoriasis treatment would you normally recommend for a pregnant woman?";
			break;
	
	case 5: return "What would you do with a current or prospective anti-TNF recipient who tested ANA 1:320 and ANA-H 1:40?";
			break;
	
	case 6: return "Does the situation warrant a review of the current psoriasis therapy?";
			break;
	
	
	
	case 7: return "According to the infliximab label [Infliximab SmPC], what degree of ALT elevation absolutely requires treatment discontinuation?";
			break;
	
	case 8: return "a. In the REVEAL study*, what proportion of patients assessed 8 weeks after treatment initiation with adalimumab had a PASI 75 response?";
			break;
	
	case 9: return "b. In the REVEAL study*, what proportion of patients assessed 12 weeks after treatment initiation with adalimumab scored 0 or 1 (cleared or minimal disease) on the PGA (Physician's Global Assessment)?";
			break;
	
	case 10: return "a. In the PHOENIX 1 study, what proportion of patients assessed 12 weeks after treatment initiation with ustekinumab had a PASI 75 response?";
			break;
	
	case 11: return "b. In the PHOENIX 1 study, what proportion of patients assessed 12 weeks after treatment initiation with ustekinumab scored 0 or 1 (cleared or minimal disease) on the PGA (Physician's Global Assessment)?";
			break;
	
	
	
	}

}


function LookupAnswers($i) {

	switch($i) {
	
	case 16: return "A: Always";
			break;

	case 17: return "B: Never";
			break;

	case 18: return "C: Sometimes";
			break;

	case 19: return "A: Yes";
			break;

	case 20: return "B: No";
			break;
			
	case 21: return "C: Don't know";
			break;

	case 22: return "A: Begin with the induction dose";
			break;

	case 23: return "B: Immediately resume maintenance dosing";
			break;

	case 24: return "A: No treatment";
			break;

	case 25: return "B: nbUVB";
			break;

	case 26: return "C: CSA";
			break;

	case 27: return "D: Topical clobetasol";
			break;

	case 28: return "E: Other";
			break;

	case 29: return "A: Do not treat with anti-TNF treatment; start treatment with antimalarials";
			break;

	case 30: return "B: If SLE symptoms are absent, proceed with anti-TNF";
			break;

	case 31: return "C: If SLE symptoms are absent, proceed with anti-TNF plus methotrexate";
			break;

	case 32: return "D: Switch to ustekinumab";
			break;

	case 33: return "E: Other";
			break;

	case 34: return "A: Yes";
			break;

	case 35: return "B: No";
			break;
			
	case 1: return "A: =3 times upper limit of normal";
			break;
			
	case 2: return "B: =4 times upper limit of normal";
			break;

	case 3: return "C: =5 times upper limit of normal";
			break;

	case 4: return "A: 26%";
			break;

	case 5: return "B: 37%";
			break;

	case 6: return "C: 54%";
			break;

	case 7: return "A: 50%";
			break;

	case 8: return "B: 60%";
			break;

	case 9: return "C: 70%";
			break;

	case 10: return "A: 30-31%";
			break;
			
	case 11: return "B: 55-56%";
			break;

	case 12: return "C: 66-67%";
			break;

	case 13: return "A: 52-54%";
			break;

	case 14: return "B: 60-62%";
			break;

	case 15: return "C: 76-78%";
			break;
			
			
	}
}



?>
</body>
