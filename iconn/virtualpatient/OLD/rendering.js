
function formatDate(currentDate) {
		
		var monthName=new Array(12);
		
		monthName[0]="January";
		monthName[1]="February";
		monthName[2]="March";
		monthName[3]="April";
		monthName[4]="May";
		monthName[5]="June";
		monthName[6]="July";
		monthName[7]="August";
		monthName[8]="September";
		monthName[9]="October";
		monthName[10]="November";
		monthName[11]="December";

		
  		var day = currentDate.getDate();
  		var month = currentDate.getMonth();
  		var year = currentDate.getFullYear();
  		return "<b>" + day + " " + monthName[month] + " " + year + "</b>";								
    
	}
	
	function formatTime(currentTime, s) {
		
  	 	var hours = currentTime.getHours();
  	 	var minutes = currentTime.getMinutes();
	 	var seconds = currentTime.getSeconds();

  		if (minutes < 10)
  			minutes = "0" + minutes;
			
		if (seconds < 10)
  			seconds = "0" + seconds;

		if(s)
  			return "<b>" + hours + ":" + minutes + "</b>";
		else
			return "<b>" + hours + ":" + minutes + "</b>:" + seconds + " ";
		
	}
	
function ShowPasi(xml, node, idToFill, history) {
	
var itemTags = xml.getElementsByTagName ("node");

	for (var i = 0; i < itemTags.length; i++) {
		 
		if(itemTags[i].getAttribute('id') == node) {
	
			var pasi = itemTags[i].getElementsByTagName("pasi")[0].textContent;
			
			if(pasi	== "") {		
			
				var v = GetVisitForNode(xml, node);
				var va = getVisit(v-1);
				ShowPasi(xml, va[1], idToFill, history)
				return;
			}
			
			document.getElementById(idToFill).innerHTML = '<div class="pasiArea">PASI: '+pasi+'</div>';
	
		}
	}
}


function ShowDosing(xml, node, idToFill) {
	
var itemTags = xml.getElementsByTagName ("node");

	for (var i = 0; i < itemTags.length; i++) {
		 
		if(itemTags[i].getAttribute('id') == node) {
	
			var dosing = itemTags[i].getElementsByTagName("dosing")[0].textContent;
			if(dosing != "")
			document.getElementById(idToFill).innerHTML = '<div class="dosing" ><font size="6"><b>Dosing information</b></font><br><br>'+dosing+'</div>';
	
		}
	}
}

function GetFaceForNode(xml, node) {
	
	var itemTags = xml.getElementsByTagName ("node");

	for (var i = 0; i < itemTags.length; i++) {
		 
		if(itemTags[i].getAttribute('id') == node) 
			return itemTags[i].getElementsByTagName("face")[0].textContent;
	}
	
	return null;
}

function ShowPageBody(xml, node, idToFill, history) {
				
	var itemTags = xml.getElementsByTagName ("node");

	for (var i = 0; i < itemTags.length; i++) {
		 
		if(itemTags[i].getAttribute('id') == node) {
	
			var str = itemTags[i].getElementsByTagName("body")[0].textContent;
			
			var face;
			
			face = itemTags[i].getElementsByTagName("face")[0].textContent;
			
			if(face	== "") {		
			
				var v = GetVisitForNode(xml, node);
				var va = getVisit(v-1);
				face = GetFaceForNode(xml, va[1]);
				
			}
			
			var visit = itemTags[i].getElementsByTagName("visit")[0].textContent;
			var pasi = itemTags[i].getElementsByTagName("pasi")[0].textContent;
	
			var faceSize;
			
			if(history)
				faceSize = 'smallFace';
			else
				faceSize = 'face';
			
	  		var insertBR = str.replace(/\n/g, "<br />");
			var htmlToInsert = '<div class="title">VISIT <span id="visitNumber">'+visit+'</span><font size="3"> ('+node+')</font></div>'+'<img class="'+faceSize+'" src="'+face+'.png">'+insertBR;
			document.getElementById(idToFill).innerHTML = htmlToInsert;
	
		}
	}
}

		
			
		   
		   
function ShowSupportingDocsList(xml, node, idToFill, history) {
				
	 var itemTags = xml.getElementsByTagName ("node");

	for (var i = 0; i < itemTags.length; i++) {
		 
		if(itemTags[i].getAttribute('id') == node) {
		
			nodeImagesHTML = "";
			var nodeImagesItems = itemTags[i].getElementsByTagName ("image");
		
			for (var k = 0; k < nodeImagesItems.length; k++) {
			
				if(k == 0)
					nodeImagesHTML += '<div onclick="parent.location=\''+nodeImagesItems[k].getElementsByTagName ("path")[0].textContent+'.html\'"><table border="0">';	
				
				if(history)
					imageClass = 'imageOnTheRightSmall';
				else
					imageClass = 'imageOnTheRight';
				
				
				
				nodeImagesHTML += '<tr><td style="width:100%; padding:10px">'+
				//nodeImagesItems[k].getElementsByTagName ("dida")[0].textContent + 
				//	'</td><td>
				'<div class="'+imageClass+'" style="background-image:url('+
				nodeImagesItems[k].getElementsByTagName ("path")[0].textContent +
					'.jpg);"></td></tr>';
		
				if(k == (nodeImagesItems.length-1))
					nodeImagesHTML += '</table></div>';	
			}
		
			document.getElementById(idToFill).innerHTML = nodeImagesHTML;
		}
	}
}
		
function pleaseDecide(userID, visitID, node, destnode, answer) {
		
	document.getElementById('confirmationBoxID').innerHTML = "YOUR RECOMMENDATION IS <br> <br>"+GetAnswerText(answer) + "<br><br>" +
	
	'<div class="option" onclick="'+'setVisit('+userID+', '+visitID+', '+node+', '+answer+', \'user\');'+'setVisit('+userID+', '+(visitID+1)+', '+destnode+', null, \'open\'); thanks('+answer+');">CONFIRM</div>';
	
	document.getElementById('confirmationBoxID').innerHTML += 
	
	'<div class="option" onclick="'+'document.getElementById(\'all\').style.display = \'block\';'+'document.getElementById(\'confirmationBoxID\').style.display = \'none\'; ">CANCEL</div>';
										
		document.getElementById('all').style.display = 'none';
		document.getElementById('confirmationBoxID').style.display = 'block';
				
}
		
function ShowOptionList(xml, node, idToFill, idContainer) {
	
	 
    var itemTags = xml.getElementsByTagName ("link");
			
	var connection;
	var optionHTML = "<br>";
	var pasi;
	
	var nOptions = 0;
	
	
	
    for (var i = 0; i < itemTags.length; i++) {
				
		connection = itemTags[i].getAttribute('connection').split(":")	 
		
		if(connection[0] == node) {
			
			nOptions++;
			
			var answer = itemTags[i].getAttribute('id');
			
			optionHTML += '<div class="option" onclick="'+
						'pleaseDecide('+userID+', '+visitID+', '+connection[0]+', '+connection[1]+', '+answer+');">'+itemTags[i].getElementsByTagName ("option")[0].textContent+
						"</div>";
				
						
		}
			
	}

	if(nOptions == 0) {
		document.getElementById(idContainer).style.display = 'none';
		return;	
	}
	else
		document.getElementById(idToFill).innerHTML = optionHTML;
			
}
	
	
var introText = "At the last visit your patient tested positive for latent TB. Test your knowledge on this important topic with the following quiz. The answers will be provided the next time your patient visits!";

var closingRemarks = "Juan presented with unstable angina, which had deteriorated by his last visit to your clinic. This fact may have influenced your last prescribing decision. This issue will be discussed further in the final session of the day.";


function ShowClosingRemarks() {
	
		clearTimeout(timeoutTimer);
		document.getElementById('recommendation').innerHTML = "Psoriasis therapy and cardiovascular disease";
		document.getElementById('options').innerHTML += '<div>'+closingRemarks+'</div>';
		document.getElementById('optionContainer').style.display = 'block';
		
}

function ShowQuestionsList(xml, node, idToFill, idContainer, user, visit) {
	
	 
    var itemTags = xml.getElementsByTagName ("test");
			
	var connection;
	var questionsHTML = "<br>";
	var pasi;
	
	var nQuestions = 0;
	
	
	
    for (var i = 0; i < itemTags.length; i++) {
					
	//	if(node == itemTags[i].getAttribute('node')) {
			
			nQuestions++;
			
			//var answer = itemTags[i].getAttribute('id');
			/*
			questionsHTML += '<div class="option" onclick="'+
						'pleaseDecide('+userID+', '+visitID+', '+connection[0]+', '+connection[1]+', '+answer+');">'+itemTags[i].getElementsByTagName ("option")[0].textContent+
						"</div>";
			*/
			questionsHTML += '<div>'+itemTags[i].getElementsByTagName ("question")[0].textContent+
						"</div><br>";
			var answerTags = itemTags[i].getElementsByTagName ("answer"); 
			
			var idq = itemTags[i].getAttribute('id');
			
			var questionsHTML1 = '<div  id="'+idq+'all">';
			var questionsHTML2 = "";
			
			for (var z = 0; z < answerTags.length; z++) {
				 
				 var ida = answerTags[z].getAttribute('id');
				 
				questionsHTML1 += '<div onclick="document.getElementById(\''+idq+'all\').style.display = \'none\'; document.getElementById(\''+ida+'single\').style.display = \'block\';" class="option">'+answerTags[z].textContent+
						"</div><br>";
				questionsHTML2 += '<div class="optionselected" id="'+ida+'single"><b>'+answerTags[z].textContent+
						"</b></div>";
		
			 }
				
			questionsHTML += questionsHTML1+"</div>"+questionsHTML2;
					
	//	}
			
	}

	if(nQuestions == 0) {
		document.getElementById(idContainer).style.display = 'none';
		return;	
	}
	else {
	
		document.getElementById(idContainer).style.display = 'none';	
		document.getElementById('optionContainer').style.display = 'block';
	    document.getElementById('recommendation').innerHTML = "";
	 
		document.getElementById('options').innerHTML += '<div>'+introText+'</div><br><div class="option" onclick="'+
		"document.getElementById('questionContainer').style.display = 'block';"+
		 "document.getElementById('all').style.display = 'none'; scroll(0,0);"+
						'">TEST YOUR KNOWLEDGE</div>';
					
		questionsHTML += '<div class="option" onclick="'+
		"document.getElementById('questionContainer').style.display = 'none';"+
		"thanksAfterTest();"+
						'">CONFIRM</div>';
		/*
		questionsHTML += '<div class="option" onclick="'+
		"document.getElementById('questionContainer').style.display = 'none';"+
		"setVisit("+user+", "+visit+", "+node+", 999999, \'random\');"+"setVisit("+user+", 5, 22, null, \'open\');"+
		 "thanksAfterTest();"+
						'">CONFIRM</div>';
		*/
		
		setVisit(user, visit, node, 999999, 'system');
		setVisit(user, 5, 22, null, 'open');
								
		document.getElementById(idToFill).innerHTML = questionsHTML;
		
						
	}
}
	


function ShowQuestionsAnswers(xml, node, idToFill, idContainer) {
	 
    var itemTags = xml.getElementsByTagName ("test");
			
	var connection;
	var questionsHTML = "<br>";
	var pasi;
	
	var nQuestions = 0;
	
	
	
    for (var i = 0; i < itemTags.length; i++) {
					
	//	if(node == itemTags[i].getAttribute('node')) {
			
			nQuestions++;
			
			//var answer = itemTags[i].getAttribute('id');
			/*
			questionsHTML += '<div class="option" onclick="'+
						'pleaseDecide('+userID+', '+visitID+', '+connection[0]+', '+connection[1]+', '+answer+');">'+itemTags[i].getElementsByTagName ("option")[0].textContent+
						"</div>";
			*/
			questionsHTML += '<div>'+itemTags[i].getElementsByTagName ("question")[0].textContent+
						" ";
	
			var answerTags = itemTags[i].getElementsByTagName ("answer"); 
			
			for (var z = 0; z < answerTags.length; z++) {
				 
				if(answerTags[z].getAttribute('value') == "1")
					questionsHTML += '<b>'+answerTags[z].textContent+
						"</b></div><br>";
	//			else
				//	questionsHTML += '<div >'+answerTags[z].textContent+
		//				"</div><br>";
				
			 }
				
						
	//	}
			
	}

	if(nQuestions == 0) {
		document.getElementById(idContainer).style.display = 'none';
	return;	
	}
	else {
	
	
	
		document.getElementById('all').style.display = 'none';
		document.getElementById('questionAnswersContainer').style.display = 'block';
	   
		
		questionsHTML += '<div class="option" onclick="'+
		"document.getElementById('all').style.display = 'block';"+
		 "document.getElementById('questionAnswersContainer').style.display = 'none';"+
						'">GO TO VISIT</div>';
							
		document.getElementById(idToFill).innerHTML = questionsHTML;
	}
	
}
	
	
	
function GetAnswerTextForNodeAndIndex(xml, visit, node, answer, origin) {
		
		var k = 0;
		
		itemTags = xml.getElementsByTagName ("link");
		
		for (var i = 0; i < itemTags.length; i++) {
		
			if(itemTags[i].getAttribute('id') == answer) {	
		
						if(origin == "user") {
							  
						  var returnHTML =  '<div class="greyTextArea" id="'+node+"aa"+'" onclick="\
							  document.getElementById(\''+node+'cc'+'\').style.display=\'block\';\
							  document.getElementById(\''+node+'aa'+'\').style.display=\'none\';\
							  "><b>VISIT '+visit+'</b>: '+itemTags[i].getElementsByTagName ("option")[0].textContent+'</div>\
							  <div  class="greyTextArea" style="display:none;" id="'+node+"cc"+'" onclick="\
							  document.getElementById(\''+node+'aa'+'\').style.display=\'block\';\
							  document.getElementById(\''+node+'cc'+'\').style.display=\'none\';\
							  "><div id="'+node+"bb"+'"></div><br><div id="'+node+"pasi"+'"></div><br><b>SUPPORTING DOCS:</b><br><br><div id="'+node+"docs"+'"></div><br>';
							  
							  returnHTML += '<b>YOUR RECOMMENDATION WAS:</b><br><br>';
								  
							  returnHTML += itemTags[i].getElementsByTagName ("option")[0].textContent+"</div>";
						}
						else {
							
							var returnHTML =  '<div class="greyTextArea" id="'+node+"aa"+'" onclick="\
					document.getElementById(\''+node+'cc'+'\').style.display=\'block\';\
					document.getElementById(\''+node+'aa'+'\').style.display=\'none\';\
					">You missed <b>VISIT '+visit+'</b>. However, the patient was seen by a colleague of yours, who recommended '+itemTags[i].getElementsByTagName ("option")[0].textContent+'</div>\
					<div  class="greyTextArea" style="display:none;" id="'+node+"cc"+'" onclick="\
					document.getElementById(\''+node+'aa'+'\').style.display=\'block\';\
					document.getElementById(\''+node+'cc'+'\').style.display=\'none\';\
					"><div id="'+node+"bb"+'"></div><br><div id="'+node+"pasi"+'"></div><br><b>SUPPORTING DOCS:</b><br><br><div id="'+node+"docs"+'"></div><br>';
					
					returnHTML += '<b>YOUR COLLEAGUE RECOMMENDED:</b><br><br>';
						
					returnHTML += itemTags[i].getElementsByTagName ("option")[0].textContent+"</div>";
	
					return returnHTML;
						}
					return returnHTML;
				
			}
		}
	/*	else {
			
				var advice = GetDefaultAnswerForVisit(xml, visit);
				
				var returnHTML =  '<div class="greyTextArea" id="'+node+"aa"+'" onclick="\
					document.getElementById(\''+node+'cc'+'\').style.display=\'block\';\
					document.getElementById(\''+node+'aa'+'\').style.display=\'none\';\
					">You missed <b>VISIT '+visit+'</b>. However, the patient was seen by a colleague of yours, who recommended '+advice+'</div>\
					<div  class="greyTextArea" style="display:none;" id="'+node+"cc"+'" onclick="\
					document.getElementById(\''+node+'aa'+'\').style.display=\'block\';\
					document.getElementById(\''+node+'cc'+'\').style.display=\'none\';\
					"><div id="'+node+"bb"+'"></div><br><div id="'+node+"pasi"+'"></div><br><b>SUPPORTING DOCS:</b><br><br><div id="'+node+"docs"+'"></div><br>';
					
					returnHTML += '<b>YOUR COLLEAGUE RECOMMENDED:</b><br><br>';
						
					returnHTML += advice+"</div>";
	
					return returnHTML;
		}
		
		*/			
				
		//}
}
	
function ShowHistory(xml, htmlToFill, historybox) {

	var	visitTimeTC = new Array();
	var outputHTML = "";
	
	var visTime = localStorage.getItem("visitTiming");
	if(visTime) 
	  	visitTimeTC = visTime.split(",")
	 
	var currentTime = new Date();
	var currentTimeTC = currentTime.getTime();
	
	var v;
	
	var somethingToShow = false;
	
	for(var i = 1; i < visitTimeTC.length-1; i++) {
		v = getVisit(i);
		if(v) {
				
				if(v[2]) {
					
					if(v[2] < 10000) {
						
						//if(v[3] == 'user') {
							  htmlToFill.innerHTML += 
							  GetAnswerTextForNodeAndIndex(xml, i,  v[1], v[2], v[3]);
							  ShowPageBody(xml, v[1], v[1]+"bb", true);
							  ShowPasi(xml, v[1], v[1]+"pasi", true);
							  ShowSupportingDocsList(xml, v[1], v[1]+"docs", true);
							  somethingToShow = true;
				//		}
					
						
					}
					else {
						htmlToFill.innerHTML += TookTheTest(xml, i,  v[1], v[2]);
						somethingToShow = true;
					
					}
				}
				
				else
				
			if((i != CurrentVisit) && (currentTimeTC > visitTimeTC[i])) {
			
				if(CurrentVisit < 6) {
					htmlToFill.innerHTML += 
					GetAnswerTextForNodeAndIndex(xml, i,  v[1], v[2], v[3]);
					ShowPageBody(xml, v[1], v[1]+"bb", true);
					ShowPasi(xml, v[1], v[1]+"pasi", true);
					ShowSupportingDocsList(xml, v[1], v[1]+"docs", true);
					
					somethingToShow = true;
				}
			}
			
		}
		/*
		else {
			
			var nod = GetDefaultNodeForVisit(xml, i);
			
			if((i != CurrentVisit) && (currentTimeTC > visitTimeTC[i])) {
			
					htmlToFill.innerHTML += 
					GetAnswerTextForNodeAndIndex(xml, i, nod, "", "system");
					ShowPageBody(xml,i, nod+"bb", true);
					ShowPasi(xml, i, nod+"pasi", true);
					ShowSupportingDocsList(xml, i, nod+"docs", true);
			
				somethingToShow = true;
			}
			
			
				
	}		*/
	
	}
	
	if(somethingToShow)
		historybox.style.display='block';
	else
		historybox.style.display='none';
	
			
}
	
function GetVisitForNode(xml, node) {
		
		var itemTags = xml.getElementsByTagName ("node");

		for (var i = 0; i < itemTags.length; i++) {
		 
			if(itemTags[i].getAttribute('id') == node) {
		
				var visit = itemTags[i].getElementsByTagName("visit")[0].textContent;
				return visit;
				
			}
		}
		
}
	
	
	
	function GetDefaultNodeForVisit(xml, n) {
		
		
		
			alert("GetDefaultNodeForVisit");
			
		
	
	
	
		
	}
	
	
	function GetDefaultAnswerForVisit(xml, n) {
	
		alert("GetDefaultAnswerForVisit");
		
		
		
	}
	
	function GetAnswerText(n) {
		
		var itemTags = xmlDoc.getElementsByTagName ("link");	
		var connection;
			
    	for (var i = 0; i < itemTags.length; i++) {
						
			if(itemTags[i].getAttribute('id')  == n) 
				return itemTags[i].getElementsByTagName ("option")[0].textContent;
		
		}
		
		return null;
	}
	
	


function TookTheTest(xml, visit, node, answer) {
		
		var k = 0;
		
		itemTags = xml.getElementsByTagName ("link");
		
		for (var i = 0; i < itemTags.length; i++) {
		
		if(answer) {
			if(itemTags[i].getAttribute('id') == answer) {	
					
				var returnHTML =  '<div class="optionsdisabled" id="'+node+"aa"+'" ><b>VISIT '+visit+'</b>: '+itemTags[i].getElementsByTagName ("option")[0].textContent+'</div>';
					
	
					return returnHTML;
				
			}
		}
		}
}
	
function GetRandomNodeLinkedToNode(startingNode) {
	
	var optionsToChoose = new Array();
	var u = 0;
	
	var itemTags = xmlDoc.getElementsByTagName ("link");	
		
		var connection;
			
    	for (var i = 0; i < itemTags.length; i++) {
				
			connection = itemTags[i].getAttribute('connection').split(":")	 
				
			if(connection[0] ==startingNode) 
				optionsToChoose[u++] = connection[1];
		
		}
		
		if(u > 0) {
			var randomnumber=Math.floor(Math.random()*u)
			return optionsToChoose[randomnumber];
		}
		else
			return null;
}

function GetAnswerLeadingFromNodeToNode(startingNode, destNode) {
	
	var itemTags = xmlDoc.getElementsByTagName ("link");	
		
		var connection;
			
    	for (var i = 0; i < itemTags.length; i++) {
				
			connection = itemTags[i].getAttribute('connection').split(":")	 
				
			if((connection[0] == startingNode) &&  (connection[1] == destNode))
				return itemTags[i].getAttribute('id');
		
		}
		
		return null;
}



		


function ShowLocalStorage() {
	
	var	visitTimeTC = new Array();
	
	var visTime = localStorage.getItem("visitTiming");
	
	if(visTime) 
	  	visitTimeTC = visTime.split(",")
	 
	var v;
	
	for(var i = 1; i < visitTimeTC.length-1; i++) {
	
		v = getVisit(i);
	
		if(v) {
			document.getElementById('testingInfo').innerHTML +=
				"<br><b>VISIT "+i+"</b>: [n:"+v[1]+"][ans:"+v[2]+"][s:"+v[3]+"][c:"+v[4]+"]<br>";

		}
	}
			
}



function RebuildHistoryWithMissingVisits(user) {
	
	if(CurrentVisit == 1)
		return;
	
	var snode, dnode;
	var vanswer;
		
	for(var i = 1; i < CurrentVisit; i++) {
			
		v = getVisit(i);
		
		if(v) {
	
				if(!v[2]) {
	
					snode = v[1];
					dnode  = GetRandomNodeLinkedToNode(v[1]);
			
					vanswer = GetAnswerLeadingFromNodeToNode(snode, dnode);
			
					if(i != 4) {
						setVisit(user, i, snode, vanswer, 'system');
						setVisit(user, i+1, dnode, null, 'open');
					}
					
					
				}
		} else {
		
		
			if(i == 1)
				snode  = 1;
			//else
				//alert("anomalia");
				
			dnode  = GetRandomNodeLinkedToNode(snode);
			
			vanswer = GetAnswerLeadingFromNodeToNode(snode, dnode);
					
			setVisit(user, i, snode, vanswer, 'system');
			setVisit(user, i+1, dnode, null, 'open');
		}
	}
}
