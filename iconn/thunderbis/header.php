<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<title></title>
<head>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />

<!--CSS-->
<link href="css/style.css" type="text/css" rel="stylesheet" />


<!--JAVASCRIPT-->
<!--<script src="javascript/functions.js" type="text/javascript"></script>-->
<!--<SCRIPT src="jquery-1.4.2.js"></SCRIPT>-->
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js" charset="utf-8"></script>
<SCRIPT src="javascript/jquery-te-1.4.0.min.js"></SCRIPT>
<!--OTHER-->
<link rel="apple-touch-icon" href="homescreen.png"/>
<link href="startup.png" rel="apple-touch-startup-image" />
<link href="css/jquery-te-1.4.0.css" type="text/css" rel="stylesheet" />
</head>




<?php


	
include("../phpservices/connect.php");


	
include("language.php");
include("config.php");

//echo '<font color="#FFFFFF" size="5">&nbsp;iCon Thunder</font><br>';


function translatecontent($id, $table, $lang) {
	
	global $db;
	
	$row = NULL;
	
if($table == "questions") {

	$q =  "SELECT * FROM tbltranslations WHERE fkid  = '".$id."' AND stable = '".$table."' AND slanguage = '".$lang."'";
	
	$row = $db->GetRow($q);	
}

if($table == "answers") {

	$q =  "SELECT * FROM tbltranslations WHERE fkid  = '".$id."' AND stable = '".$table."' AND slanguage = '".$lang."'";
	
	$row = $db->GetRow($q);	
}

	if($row) 
		return $row['stranslation'];
	else
		return NULL;
}



if(isset($_GET['meeting']))
	$MEETING = $_GET['meeting'];
else
	$MEETING = 0;


if(isset($_GET['room']))
	$ROOM = $_GET['room'];
else
	$ROOM = 0;

if(isset($_GET['questionlanguage']))
	$QUESTIONLANGUAGE = $_GET['questionlanguage'];
else
	$QUESTIONLANGUAGE = 0;

if(isset($_GET['questionGroup']))
	$QUESTIONGROUP = $_GET['questionGroup'];
else
	$QUESTIONGROUP = 0;
	
if(isset($_GET['questionGroupTwitter']))
	$QUESTIONGROUPTWITTER = $_GET['questionGroupTwitter'];
else
	$QUESTIONGROUPTWITTER = 0;
	
// SELEZIONA MEETING

//DANILO PER EVENTO ASTELLAS ROMA 28_10_2014

if(isset($_GET['partecipant']))
	$PARTECIPANT = $_GET['partecipant'];
else
	$PARTECIPANT = 0;


$FORM_INCLUDE = "";

$GAMEPARTECIPANTS['0'] = "";
$MEETINGS['0'] = "";
$ROOMS['0'] = "";


$MEETINGANDROOM_INCLUDE = "<input type='hidden' name='meeting' value='".$MEETING."'>
			<input type='hidden' name='room' value='".$ROOM."'>
			<input type='hidden' name='questionlanguage' value='".$QUESTIONLANGUAGE."'>
			<input type='hidden' name='questionGroupTwitter' value='".$QUESTIONGROUPTWITTER."'>
			<input type='hidden' name='questionGroup' value='".$QUESTIONGROUP."'>";


if($SHOW_SELECTORS == '1') {
		if(basename($_SERVER['PHP_SELF'])!="index.php")
	 	  echo "<ul class=\"pageitem\"><li class=\"button\">
		    <input name=\"action\" type=\"button\" value=\"Thunderbis HOME\" onclick=\"window.location.href='index.php'+window.location.search\">
		  </li></ul>";

	  	  echo "<br>";
	  	  echo "<form id=\"meetingandroom\" name=\"meetingandroom\" action='".$_SERVER['PHP_SELF']."' method='get'>";
	  
		  $meetingsQuery = "SELECT * FROM tblmeetingentity ORDER BY pkidmeeting DESC"; 
			  
		  $meetingsRows = $db->GetAll($meetingsQuery);	
		  
		  //echo '<div id="content">';
		  
		  if(count($meetingsRows) > 0){
			  
			  echo '<ul class="pageitem">';
			  echo '<li class="select">';
			  echo '<select name="meeting" id="meeting" onChange="document.getElementById(\'meetingandroom\').submit();">';
			  echo '<option value="0">&nbsp;&nbsp;Select a meeting</option>';
			  
			  foreach($meetingsRows as $meetingsRow) {
		  
				  $MEETINGS[$meetingsRow['pkidmeeting']] = $meetingsRow['sname'];
				  
				  echo '<option value="'.$meetingsRow['pkidmeeting'].'"';
				  
				  if($MEETING == $meetingsRow['pkidmeeting'])
					  echo " selected ";
					  
				  echo '>&nbsp;&nbsp;'.$meetingsRow['sname'].'</option>
				  ';
	  
			  }
			  
			  echo '</select>';
			  echo '<span class="arrow"></span>';
			  echo '</li></ul>';
		  }
	  
	  // SELEZIONA STANZA
	  
		  $roomsQuery = "SELECT * FROM tblroomentity ORDER BY sname ASC";
			  
		  //echo "<pre>".$roomsQuery;
			  
		  $roomsQueryRows = $db->GetAll($roomsQuery);	
		  
		  //echo '<div id="content">';
		  
		  if(count($roomsQueryRows) > 0) {
			  
			  echo '<ul class="pageitem">';
			  echo '<li class="select">';
			  echo '
			  
			  <select name="room" id="room" onChange="document.getElementById(\'meetingandroom\').submit();">';
			  echo '<option value="0">&nbsp;&nbsp;Select a room</option>';
			  
			  foreach($roomsQueryRows as $roomsQueryRow) {
						  
				  $ROOMS[$roomsQueryRow['pkidroom']] = $roomsQueryRow['sname'];
				  
				  if($roomsQueryRow['fkidmeeting'] == $MEETING) {
				  
						  echo '<option value="'.$roomsQueryRow['pkidroom'].'"';
				  
						  if($ROOM == $roomsQueryRow['pkidroom'])
							  echo " selected ";
					  
						  echo '>&nbsp;&nbsp;'.$roomsQueryRow['sname'].'</option>
						  ';
				  }
			  }
		  
			  
			  echo '</select>';
			  echo '<span class="arrow"></span>';
			  echo '</li></ul>';
			  
				  
		    }
	  // SELEZIONA SESSIONE
	  
		  if($ROOM > 0)
		  $questionsQuery = "SELECT * FROM tblquestiongroups LEFT JOIN tblquestiongroupsevent_map ON tblquestiongroups.pkidquestiongroup =  tblquestiongroupsevent_map.pkidquestiongroup WHERE pkidroom = '".$ROOM."' ORDER BY norder ASC"; 
		  else
		  $questionsQuery = "SELECT * FROM tblquestiongroups ORDER BY norder ASC"; 

		  $toAddForTwitter = "";
		  
		  if($QUESTIONGROUP > 0) {
			
			$whichSession = "SELECT * FROM tblquestiongroups WHERE pkidquestiongroup ='".$QUESTIONGROUP."'";
	$qg = $db->GetRow($whichSession);
	
	$toAddForTwitter = "<input type='hidden' name='questionGroupTwitter' value='".$qg['fkIdTweetingSession']."'>";
		
	echo $toAddForTwitter;
			if(isset($qg['fkIdTweetingSession'])&&$qg['fkIdTweetingSession']!=''&&$qg['fkIdTweetingSession']!=0)
				echo "&nbsp;TWEET SESSION: ".$qg['fkIdTweetingSession']."<br><br>";
			else
				echo "&nbsp;NO TWEET SESSION<br><br>";
			
		  }
		  
		  $questionsRows = $db->GetAll($questionsQuery);	
		  
		  //echo '<div id="content">';
		  
		  if(count($questionsRows) > 0) {
			  
			  echo '<ul class="pageitem">';
			  echo '<li class="select">';
			  echo '
			  
			  <select name="questionGroup" id="questionGroup" onChange="document.getElementById(\'meetingandroom\').submit();">';
			  echo '<option value="0">&nbsp;&nbsp;Select a session</option>';
			  
			  foreach($questionsRows as $questionsRow) {
		  
		  
				  echo '<option value="'.$questionsRow['pkidquestiongroup'].'"';
				  
				  if($QUESTIONGROUP == $questionsRow['pkidquestiongroup']) 
					  echo " selected ";
					  
				  echo '>&nbsp;&nbsp;'.$questionsRow['stitle'].'</option>
				  ';
	  
			  }
			  
			  echo '</select>';
			  echo '<span class="arrow"></span>';
			  echo '</li></ul>';
		  }
	
// SELEZIONA LINGUA
	
	echo '<ul class="pageitem">';
	echo '<li class="select">';
	echo '
		
		<select name="questionlanguage" id="questionlanguage" onChange="document.getElementById(\'meetingandroom\').submit();">';
		
			echo '<option value="en-GB"';
			
			if($QUESTIONLANGUAGE == "en-GB")
				echo " selected ";
				
			echo '>&nbsp;&nbsp;English</option>
			';

			echo '<option value="it-IT"';
			
			if($QUESTIONLANGUAGE == "it-IT")
				echo " selected ";
				
			echo '>&nbsp;&nbsp;Italiano</option>
			';
		
		echo '<option value="0"';
		
			if($QUESTIONLANGUAGE == "0")
				echo " selected ";
			
			echo '>&nbsp;&nbsp;Select a language</option>';
	
	
		echo '</select>';
    	echo '<span class="arrow"></span>';
    	echo '</li></ul>';
	

	echo "</form>";
	

}



//echo "<pre>".print_r($ROOMS);
//echo "<pre>".print_r($MEETINGS);

/*
echo '<li class="checkbox"><span class="name">Auto refresh</span>';

			echo '<input name="autorefresh" id="autorefresh" type="checkbox" '; 
			if($autorefresh == "on")
				echo " checked ";
			echo 'onclick="document.forms[\'autorefr\'].submit()">';
			echo  '</li>';
*/
			
//echo "</ul></form>";

if(isset($_GET['autorefresh']))
		$AUTOREFRESH = $_GET['autorefresh'];
	else
		$AUTOREFRESH = "";

if(isset($_GET['action']))
		$ACTION = $_GET['action'];
	else
		$ACTION = "";
		
$CURRENT_LANGUAGE = "en";

function monthbyid($id) {

global $CURRENT_LANGUAGE;

if($CURRENT_LANGUAGE == "it") {
	
	switch($id) {
	
	case  1: return "gennaio";
	case  2: return "febbraio";
	case  3: return "marzo";
	case  4: return "aprile";
	case  5: return "maggio";
	case  6: return "giugno";
	case  7: return "luglio";
	case  8: return "agosto";
	case  9: return "settembre";
	case 10: return "ottobre";
	case 11: return "novembre";
	case 12: return "dicembre";
	
	}
}

if($CURRENT_LANGUAGE == "en") {
	
	switch($id) {
	
	case  1: return "january";
	case  2: return "february";
	case  3: return "march";
	case  4: return "april";
	case  5: return "may";
	case  6: return "june";
	case  7: return "july";
	case  8: return "august";
	case  9: return "september";
	case 10: return "october";
	case 11: return "november";
	case 12: return "december";
	
	}
}
}

function formatdate($date) {

global $CURRENT_LANGUAGE;

if($CURRENT_LANGUAGE == "it") {

	if($date <> "") {
	
	
		$year = substr($date,0, 4);
		$month = substr($date,5, 2);
		$day = (int)substr($date, 8, 2);
		$timex = substr($date,13);
		$hour = (int)substr($date, 11, 2)-2;
	//	return $day." ".monthbyid($month)." ".$year." alle ".$hour.$timex;
		
		return $day." ".monthbyid($month)." alle ".$hour.$timex;
	}
	else
		return "";
}

if($CURRENT_LANGUAGE == "en") {

	if($date <> "") {
	
	
		$year = substr($date,0, 4);
		$month = substr($date,5, 2);
		$day = (int)substr($date, 8, 2);
		$timex = substr($date,13);
		$hour = (int)substr($date, 11, 2)-2;
		//return monthbyid($month)." ".$day.", ".$year.", ".$hour.$timex;
		return monthbyid($month)." ".$day.", ".$hour.$timex;

	}
	else
		return "";
}


}



		
function GetUserNameForDevice($id) {

global $db;
	
	$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");	
	
	if($row) 
		return $row['slastname']." ".$row['sfirstname'];
	else
		return "";
}


function GetNickNameForDevice($id) {

	global $db;
	$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");	
	
	if($row) 
		return $row['susername'];
	else
		return "";
}

function GetRoomForDevice($id) {

global $db;
	
	$row = $db->GetRow("SELECT * FROM tblaccess WHERE fkiddevice  = '".$id."' AND dtmlogout IS NULL ORDER BY dtmaccess DESC");	
	
	if($row) 
		return $row['fkidroom'];
	else
		return "";
}





function convertColumnToString($rs,$nColumn,$separator = null){
	$result='';
	foreach($rs as $row){
		$result.=(isset($separator)?$separator:',').$row[($nColumn?$nColumn:0)];
	}
	return substr($result, 1); 
}
	

/*
	
	if(isset($_GET['expscreen']))
		$S1 = $_GET['expscreen'];
	else
		$S1 = "";
	
	if(isset($_GET['expapproved']))
		$S2 = $_GET['expapproved'];
	else
		$S2 = "";
	
	if(isset($_GET['exptoapprove']))
		$S3 = $_GET['exptoapprove'];
	else
		$S3 = "";
	
	if(isset($_GET['autorefresh']))
		$autorefresh = $_GET['autorefresh'];
	else
		$autorefresh = "";


function GetUserNameForDevice($id) {

global $USE_DEVICE_ID;
global $db;

	if($USE_DEVICE_ID) {
		return "iPod ".$id;
	}
	else {
		$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");	
	
		if($row) 
			return $row['slastname']." ".$row['sfirstname'];
		else
			return "iPod non assegnato";
	}
}


function GetUserIdForDevice($id) {

global $db;

	$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");	
	
	if($row)
		return $row['pkiduser'];
	else
		return "";
}

function GetRoomNameFromId($id) {

global $db;

	$row = $db->GetRow("SELECT * FROM tblroomentity WHERE pkidroom  = '".$id."'");	
	
	if($row)
		return $row['sname'];
	else
		return "";
}

function GetUserNameForUserId($id, $self=0) {

global $db;

	if($id == $self)
		return translate('List:You');
		
	$row = $db->GetRow("SELECT * FROM tbluserentity WHERE pkiduser  = '".$id."'");	
	
	if($row) 
		return $row['sfirstname']." ".$row['slastname'];
	else
		return "";
}
	
	
function ShowSender($sender) {

global $ANONYMOUS;
global $USE_DEVICE_ID;

	if($ANONYMOUS) 
			echo '<span class="comment">';
		else {
			if($USE_DEVICE_ID) 
				echo '<span class="comment">'.translate('List:From').
					': '.GetUserNameForDevice($sender).'</span>';
			else 
				echo '<span class="comment">'.translate('List:From').
					': '.GetUserNameForUserId($sender).'</span>';
		}	
}

function ShowDate($date) {

	if($ANONYMOUS)
		echo '<span class="comment"><br>'./*translate('List:Date').": ".formatdate($date).'</span>';
		
		else
		echo '<span class="comment">'./*translate('List:Date').": ".formatdate($date).'</span>';
		
	// echo '<span class="starcomment"></span>';
}	


function strictify ( $string ) {
     
	$fixed = htmlspecialchars( $string, ENT_QUOTES );
	
    $trans_array = array();
    for ($i=127; $i<255; $i++) {
        $trans_array[chr($i)] = "&#" . $i . ";";
    }

    $really_fixed = strtr($fixed, $trans_array);

    return $really_fixed;
}


$quit = 0;
	
if(isset($_GET['you'])) {
	
	//echo "USING CACHE";
	
	$YOU = $_GET['you'];
	$SESSION = $_GET['session'];
	$MODERATOR = $_GET['moderator'];
	$MEETING = $_GET['meeting'];
	$ROOM = $_GET['room'];
	$DEVICE = $_GET['device'];
	$TITLE = $_GET['title'];
	
	if($_GET['you_are_speaker'] == "0)")
		$YOU_ARE_SPEAKER = 0;
	else if($_GET['you_are_speaker'] == "1)")
		$YOU_ARE_SPEAKER = 1;
	else
		$YOU_ARE_SPEAKER = $_GET['you_are_speaker'];
	
	//echo "FROM CACHE YOU ARE SPEAKER ".$YOU_ARE_SPEAKER;
}
else {

	//echo "RELODING FROM DB";
	




// E' POSSIBILE ASSOCIARE I TWEETS AL DISPOSITIVO O AGLI UTENTI


	if($USE_DEVICE_ID)
		$YOU = $_GET['device'];
	else
		$YOU = GetUserIdForDevice($_GET['device']);
	
// SE E' SPECIFICATA LA TWITTER SESSION, IGNORIAMO MEETING E ROOM

	if(isset($_GET['s'])) 
		$row = $db->GetRow("SELECT * FROM tbltweetingsessions WHERE 
						pkidTweetingSession  = '".$_GET['s']."'");	
	else
		$row = $db->GetRow("SELECT * FROM tbltweetingsessions WHERE 
						fkidRoom  = '".$_GET['room']."'
					AND fkidMeeting = '".$_GET['meeting']."'");	
	
// ESTRAE I DATI DELLA TWITTER SESSION E LI TRASFORMA IN VARIABILI DI SESSIONE

	if($row) {
	
		$SESSION = $row['pkidTweetingSession'];
		$TITLE = $row['stitle'];
		$MODERATOR = 0;
		$MODERATED = $row['bModerated'];
		if(isset($row['fkidModeratorId']))
			$MODERATOR = $row['fkidModeratorId'];
	 
		$row = $db->GetRow("SELECT * FROM tbltweetingspeakers WHERE 
						fkidTweetingSession  = '".$SESSION."'
					AND fkidspeaker = '".$YOU."'");						
		if($row)
			$YOU_ARE_SPEAKER = 1;
		else
			$YOU_ARE_SPEAKER = 0;
	}
	else
		$quit = 1;
		
	$MEETING = $_GET['meeting'];
	$ROOM = $_GET['room'];
	$DEVICE = $_GET['device'];
	
	

	
	//echo "DEVICE: ".$DEVICE." MODERATOR: ".$MODERATOR." SESSION: ".$SESSION;

}

		
if(!$quit) {

		if($MODERATOR)
			$MODERATED = 1;
		else
			$MODERATED = 0;
			
		if($YOU == $MODERATOR)
			$YOU_ARE_MODERATOR = 1;
		else
			$YOU_ARE_MODERATOR = 0;
					
		
		$FORM_INCLUDE = "
		
		<input type='hidden' name='you' value='".$YOU."'>
		<input type='hidden' name='session' value='".$SESSION."'>
		<input type='hidden' name='moderator' value='".$MODERATOR."'>
		<input type='hidden' name='meeting' value='".$MEETING."'>
		
		<input type='hidden' name='expscreen' value='".$S1."'>
		<input type='hidden' name='expapproved' value='".$S2."'>
		<input type='hidden' name='exptoapprove' value='".$S3."'>
		<input type='hidden' name='autorefresh' value='".$autorefresh."'>
		
		<input type='hidden' name='title' value='".$TITLE."'>
		<input type='hidden' name='room' value='".$ROOM."'>
		<input type='hidden' name='you_are_speaker' value='".$YOU_ARE_SPEAKER."'>
		<input type='hidden' name='device' value='".$DEVICE."'>
		";
		
		$FORM_INCLUDE_MIDDLE = "
		
		<input type='hidden' name='you' value='".$YOU."'>
		<input type='hidden' name='session' value='".$SESSION."'>
		<input type='hidden' name='moderator' value='".$MODERATOR."'>
		<input type='hidden' name='meeting' value='".$MEETING."'>
		
		<input type='hidden' name='expscreen' value='".$S1."'>
		<input type='hidden' name='expapproved' value='".$S2."'>
		<input type='hidden' name='exptoapprove' value='".$S3."'>
		
		<input type='hidden' name='title' value='".$TITLE."'>
		<input type='hidden' name='room' value='".$ROOM."'>
		<input type='hidden' name='you_are_speaker' value='".$YOU_ARE_SPEAKER."'>
		<input type='hidden' name='device' value='".$DEVICE."'>
		";
		
		$FORM_INCLUDE_SHORT = "
		
		<input type='hidden' name='you' value='".$YOU."'>
		<input type='hidden' name='session' value='".$SESSION."'>
		<input type='hidden' name='moderator' value='".$MODERATOR."'>
		<input type='hidden' name='meeting' value='".$MEETING."'>
		<input type='hidden' name='autorefresh' value='".$autorefresh."'>
		
		<input type='hidden' name='title' value='".$TITLE."'>
		<input type='hidden' name='room' value='".$ROOM."'>
		<input type='hidden' name='you_are_speaker' value='".$YOU_ARE_SPEAKER."'>
		<input type='hidden' name='device' value='".$DEVICE."'>
		";
	
}
*/
	
?>

