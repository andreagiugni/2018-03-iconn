<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Presentation control</title>
 	<meta name = "viewport" content = "width = 1024, user-scalable=no, initial-scale=1, maximum-scale=1">
    <link href="css/font/font.css" rel="stylesheet" type="text/css" />
	<style>

        * { -webkit-user-select: none; -webkit-touch-callout: none; -webkit-tap-highlight-color: transparent; background-size:100%; background-repeat:no-repeat; }
        body, html { padding: 0px; margin: 0px; font-family: sans-serif; width:890px; height:668px; position:absolute;  top:0px; left:0px; overflow:auto;-webkit-overflow-scrolling: touch}
        body{background-color:#fff; }

        .btnRiepilogo {
			font-family: 'GothamNarrow';
			font-weight: 100;
			width: 140px;
			height: 20px;
			border-radius: 10px;
			background-color: #40637F;
			-webkit-box-shadow: 0px 0px 0px 1px rgba(255, 255, 255, 1);
			position: relative;
			font-size: 16px;
			color: white;
			text-align: center;
			-webkit-user-select: none;
			margin: 10px;
			padding: 16px 20px 14px;
			display: inline-block;
		}
        
        .btn{
			font-family: 'GothamNarrow';
			font-weight:100;
            
			width: 140px;
            height: 40px;
            border-radius: 10px;
            background-color: #40637F;
            -webkit-box-shadow: 0px 0px 0px 1px rgba(255, 255, 255, 1);
            position: relative;
            font-size: 16px;
            color: white;
            text-align: center;
            -webkit-user-select: none;
            margin: 20px auto;
       		padding: 16px 0px 14px;
		}
        .btn.nascondi, .btnRiepilogo.nascondi{
			background-color: #8ea7bc;
			/*color: #40637F;*/
			}
		
		
        /*
		.btn::after{
            width: 96%;
            height: 86%;
            border-radius: 6px;
            content: '';
            background: -webkit-linear-gradient(bottom, rgba(255, 255, 255, 0.2) 0%, rgba(255, 255, 255, 0) 60%);
            position: absolute;
            left: 2%;
            top: 7%;
        }
		*/
        .btn:active{
            -webkit-box-shadow: 0px 0px 4px 0px rgba(255, 255, 255, 0.6);
        }   
        .ans{
            background:#d9dbe3;
            border-bottom: 2px dashed #40637F;
			margin-top: 1px;
		}
        .ansContainer{
			position:relative; 
			background: white; 
			padding: 40px 10px 10px; 
			margin: 20px auto;  
			width: 540px;
			white-space: pre-line;
			font-size:20px;
		}
        .ansContainer::before{
            content:'';
            position:absolute;
            top:0px;
            left:0px;
            width:100%;
            height:25px;
            border-bottom:1px solid  #40637F;
            }
        .ansContainer::after{
            content:'';
            position:absolute;
            top:7px;
            right:7px;
            width: 0;
			height: 0;
			border-style: solid;
			border-width: 0 12px 12px 12px;
			border-color: transparent transparent #40637f transparent;
            }

        .ansContainer.closed{   
            text-overflow: ellipsis;
            overflow:hidden;
       		white-space: nowrap; 
			height: 20px;
			}
		.ansContainer.closed::after{
			border-width: 12px 12px 0 12px;
			border-color: #40637f transparent transparent transparent;
            }
        .teamName{
			vertical-align: top;
			padding: 20px; 
			font-size: 20px; 
			font-weight: bold; 
			color: #40637F; 
			font-family: 'GothamNarrow';
		} 
		
		#question{
			padding: 0px 60px; 
			margin: 0px auto 40px;    
			text-align: center; 
			font-size: 40px; 
			color: #40637f;
			font-family: 'GothamNarrow';
		}
		
		.disabled{
			opacity:.15;
			}
		
		#ansContainer .ans:last-child  {
			
			border-bottom:none;
			}
    </style>
	<script type="text/javascript" src="javascript/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">

		$(function(){
			updateAnswers();
			showOnScreen();
			setInterval(updateAnswers,3000);
//		
		});
		
		function updateAnswers(){
			$.ajax({
					type: "GET",
					url: "getOpenText.php",
					dataType:"json",
					data:{
						q: <?php echo $_GET['q'];?>,
						count: <?php echo $_GET['count'];?>
					},
					success: function(data) {            
//						alert(JSON.stringify(data))  pkiduser, sgroup , ISNULL(sQuestionBody,'') as ans
						var result='';
						for(var i=0;i<data.length;i++){
							var closed=($('#ans'+data[i]["pkiduser"]).hasClass("closed")||!$('#ans'+data[i]["pkiduser"]).hasClass("ansContainer"))?"closed":"";
							var nascondi=($('#btnsend'+data[i]["pkiduser"]).hasClass("nascondi")||!$('#btnsend'+data[i]["pkiduser"]).hasClass("btn"))?"nascondi":"";							
							var TXTnascondi=($('#btnsend'+data[i]["pkiduser"]).hasClass("nascondi"))?"&lt;&lt; NASCONDI DA<BR>SCHERMO":"MOSTRA A<BR>SCHERMO &gt;&gt;";		
							var onclick= 'showOnScreen(this,'+data[i]["pkiduser"]+',1)';												
							result+='<div class="ans">'
									+'<table width="100%" cellspacing=0 cellspadding=0><tr><td class="teamName" width="15%">'+data[i]["sgroup"]+'</td>'
									+'<td width="65%" valign="top" ><div id="ans'+data[i]["pkiduser"]+'" class="ansContainer '+closed+'" onclick="$(this).toggleClass(\'closed\')">'+data[i]["ans"]+'</div></td>'
									+'<td width="20%" valign="top"><div id="btnsend'+data[i]["pkiduser"]+'" class="btn '+nascondi+(data[i]["ans"]!=""?"":" disabled")+'" onclick="'+(data[i]["ans"]!=""?onclick:"")+'">'+TXTnascondi+'</div></td>'
									+'</tr></table></div>';
						
							}
						$("#ansContainer").html(result);	
					},
					error: function() {
						alert("Server non raggiungibile");
					}
				});	
		}
		
		function showOnScreen(el, usr, toAllVal, recap){
			if(el&&$(el).hasClass("nascondi"))
				usr=0;
			$.ajax({
					type: "GET",
					url: "sendQuestionToScreen.php",
					data:{
						room: <?php echo $_GET['room'];?>,
						meeting: <?php echo $_GET['meeting'];?>,
						q: <?php echo $_GET['q'];?>,
						count: <?php echo $_GET['count'];?>,
						u:usr?usr:0,
						recap:recap?recap:0,
						toAll: toAllVal?toAllVal:0,
					},
					success: function(data) {            
						if(usr){
							$(".btn").removeClass("nascondi").html("MOSTRA A<BR>SCHERMO &gt;&gt;");
							$(el).addClass("nascondi").html("&lt;&lt; NASCONDI DA<BR>SCHERMO");
							}
						else{
							$(".btn").removeClass("nascondi").html("MOSTRA A<BR>SCHERMO &gt;&gt;");
							}
						if(recap)
							$(el).addClass("nascondi").html("INVIATO");	
						else
							$("#btnsendRiepilogo").removeClass("nascondi").html("INVIA RIEPILOGO");
							
					},
					error: function() {
						alert("Server non raggiungibile");
					}
				});	
		}
	/*
		   INSERT into tblstack (scommand, sargument, sparams, fkidroom, fkiddevice, fkidmeeting, bdelivered, bkeepon, bblock, bpersistent, dtminsert)
             VALUES ('URL', '".$template."', 'q=".$question."&count=".$id."', '".$destroom."', '999999".$destroom.", '".$MEETING."', '0', '1', '1', '1', '". $datetdm ."') 
			*/
	</script>
    
</head>
<body>
    <?php
		include("../phpservices/connect.php");
		$rw = $db->GetRow("SELECT squestion FROM tblquestionevaluationentity WHERE pkidquestion = ".$_GET['q']);
		echo "<div id='question'>".$rw['squestion']."</div>";
	?>	
        <div class="ans"><div id="btnsendRiepilogo" class="btnRiepilogo" onclick="showOnScreen(this,0,1,1)">INVIA RIEPILOGO</div></div>
    <div id="ansContainer"></div>
    
</body>
</html>