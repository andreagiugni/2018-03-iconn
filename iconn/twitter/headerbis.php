<?php

include("connect.php");
include("language.php");
include("config.php");


function monthbyid($id) {

global $CURRENT_LANGUAGE;

if($CURRENT_LANGUAGE == "it") {
	
	switch($id) {
	
	case  1: return "gennaio";
	case  2: return "febbraio";
	case  3: return "marzo";
	case  4: return "aprile";
	case  5: return "maggio";
	case  6: return "giugno";
	case  7: return "luglio";
	case  8: return "agosto";
	case  9: return "settembre";
	case 10: return "ottobre";
	case 11: return "novembre";
	case 12: return "dicembre";
	
	}
}

if($CURRENT_LANGUAGE == "en") {
	
	switch($id) {
	
	case  1: return "january";
	case  2: return "february";
	case  3: return "march";
	case  4: return "april";
	case  5: return "may";
	case  6: return "june";
	case  7: return "july";
	case  8: return "august";
	case  9: return "september";
	case 10: return "october";
	case 11: return "november";
	case 12: return "december";
	
	}
}
}

function formatdate($date) {

global $CURRENT_LANGUAGE;

if($CURRENT_LANGUAGE == "it") {

	if($date <> "") {
	
	
		$year = substr($date,0, 4);
		$month = substr($date,5, 2);
		$day = (int)substr($date, 8, 2);
		$timex = substr($date,13);
		$hour = (int)substr($date, 11, 2)-2;
		return $day." ".monthbyid($month)." ".$year." alle ".$hour.$timex;
	}
	else
		return "";
}

if($CURRENT_LANGUAGE == "en") {

	if($date <> "") {
	
	
		$year = substr($date,0, 4);
		$month = substr($date,5, 2);
		$day = (int)substr($date, 8, 2);
		$timex = substr($date,13);
		$hour = (int)substr($date, 11, 2)-2;
		return monthbyid($month)." ".$day.", ".$year." at ".$hour.$timex;
	}
	else
		return "";
}


}

function GetUserNameForDevice($id) {

global $USE_DEVICE_ID;
global $db;

	if($USE_DEVICE_ID) {
		return "iPod ".$id;
	}
	else {
		$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");	
	
		if($row) 
			return $row['slastname']." ".$row['sfirstname'];
		else
			return "iPod non assegnato";
	}
}


function GetUserIdForDevice($id) {

global $db;

	$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");	
	
	if($row)
		return $row['pkiduser'];
	else
		return "";
}

function GetRoomNameFromId($id) {

global $db;

	$row = $db->GetRow("SELECT * FROM tblroomentity WHERE pkidroom  = '".$id."'");	
	
	if($row)
		return $row['sname'];
	else
		return "";
}

function GetUserNameForUserId($id, $self=0) {

global $db;

	if($id == $self)
		return translate('List:You');
		
	$row = $db->GetRow("SELECT * FROM tbluserentity WHERE pkiduser  = '".$id."'");	
	
	if($row) 
		return $row['sfirstname']." ".$row['slastname'];
	else
		return "";
}
	
	
function ShowSender($sender) {

global $ANONYMOUS;
global $USE_DEVICE_ID;

	if($ANONYMOUS) 
			echo '<span class="comment">';
		else {
			if($USE_DEVICE_ID) 
				echo '<span class="comment">'.translate('List:From').
					': '.GetUserNameForDevice($sender).'</span>';
			else 
				echo '<span class="comment">'.translate('List:From').
					': '.GetUserNameForUserId($sender).'</span>';
		}	
}

function ShowDate($date) {

	echo '<span class="comment"><br>'./*translate('List:Date').": ".*/formatdate($date).'</span>';
	// echo '<span class="starcomment"></span>';
}	


function strictify ( $string ) {
     
	$fixed = htmlspecialchars( $string, ENT_QUOTES );
	
    $trans_array = array();
    for ($i=127; $i<255; $i++) {
        $trans_array[chr($i)] = "&#" . $i . ";";
    }

    $really_fixed = strtr($fixed, $trans_array);

    return $really_fixed;
}


$quit = 0;
	
if(isset($_GET['you'])) {
	
	//echo "USING CACHE";
	
	$YOU = $_GET['you'];
	$SESSION = $_GET['session'];
	$MODERATOR = $_GET['moderator'];
	$MEETING = $_GET['meeting'];
	$ROOM = $_GET['room'];
	$DEVICE = $_GET['device'];
	$TITLE = $_GET['title'];
	
	if($_GET['you_are_speaker'] == "0)")
		$YOU_ARE_SPEAKER = 0;
	else if($_GET['you_are_speaker'] == "1)")
		$YOU_ARE_SPEAKER = 1;
	else
		$YOU_ARE_SPEAKER = $_GET['you_are_speaker'];
	
	//echo "FROM CACHE YOU ARE SPEAKER ".$YOU_ARE_SPEAKER;
}
else {

	//echo "RELODING FROM DB";
	




// E' POSSIBILE ASSOCIARE I TWEETS AL DISPOSITIVO O AGLI UTENTI


	if($USE_DEVICE_ID)
		$YOU = $_GET['device'];
	else
		$YOU = GetUserIdForDevice($_GET['device']);
	
// SE E' SPECIFICATA LA TWITTER SESSION, IGNORIAMO MEETING E ROOM

	if(isset($_GET['s'])) 
		$row = $db->GetRow("SELECT * FROM tbltweetingsessions WHERE 
						pkidTweetingSession  = '".$_GET['s']."'");	
	else
		$row = $db->GetRow("SELECT * FROM tbltweetingsessions WHERE 
						fkidRoom  = '".$_GET['room']."'
					AND fkidMeeting = '".$_GET['meeting']."'");	
	
// ESTRAE I DATI DELLA TWITTER SESSION E LI TRASFORMA IN VARIABILI DI SESSIONE

	if($row) {
	
		$SESSION = $row['pkidTweetingSession'];
		$TITLE = $row['stitle'];
		$MODERATOR = 0;
		$MODERATED = $row['bModerated'];
		if($MODERATED && isset($row['fkidModeratorId']))
			$MODERATOR = $row['fkidModeratorId'];
	 
		$row = $db->GetRow("SELECT * FROM tbltweetingspeakers WHERE 
						fkidTweetingSession  = '".$SESSION."'
					AND fkidspeaker = '".$YOU."'");						
		if($row)
			$YOU_ARE_SPEAKER = 1;
		else
			$YOU_ARE_SPEAKER = 0;
	}
	else
		$quit = 1;
		
	$MEETING = $_GET['meeting'];
	$ROOM = $_GET['room'];
	$DEVICE = $_GET['device'];
	
	//echo "DEVICE: ".$DEVICE." MODERATOR: ".$MODERATOR." SESSION: ".$SESSION;

}

if(!$quit) {

		if($MODERATOR)
			$MODERATED = 1;
		else
			$MODERATED = 0;
			
		if($YOU == $MODERATOR)
			$YOU_ARE_MODERATOR = 1;
		else
			$YOU_ARE_MODERATOR = 0;
					
		
		$FORM_INCLUDE = "
		
		<input type='hidden' name='you' value='".$YOU."'>
		<input type='hidden' name='session' value='".$SESSION."'>
		<input type='hidden' name='moderator' value='".$MODERATOR."'>
		<input type='hidden' name='meeting' value='".$MEETING."'>
		<input type='hidden' name='title' value='".$TITLE."'>
		<input type='hidden' name='room' value='".$ROOM."'>
		<input type='hidden' name='you_are_speaker' value='".$YOU_ARE_SPEAKER."'>
		<input type='hidden' name='device' value='".$DEVICE."'>
		";
	
}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<title>Twitter</title>
<head>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />
<link href="css/style.css" type="text/css" rel="stylesheet" />
<script src="javascript/functions.js" type="text/javascript"></script>
<link rel="apple-touch-icon" href="homescreen.png"/>
<link href="startup.png" rel="apple-touch-startup-image" />
<style>
.pageitem{-webkit-border-radius:8px;background-color:#fff;border:#fff solid 1px;font-size:14pt;overflow:hidden;padding:0;position:relative;display:block;height:auto;width:auto;margin:3px 9px 17px;list-style:none}
.textbox{padding:5px 9px;position:relative;overflow:hidden;border-top:0px solid #878787}
</style>

</head>

