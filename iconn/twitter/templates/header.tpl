<title>{{$title_site}}</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge" />
{{if $browser eq 'mobile'}}
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="{{$path_css}}mobile/master.css" />

	<!--<link rel="stylesheet" href="{{$path_css}}mobile/jquery.mobile-1.0.css" />
      <link rel="stylesheet" href="{{$path_css}}mobile/jqm-docs.css" />
      <link rel="stylesheet" href="{{$path_css}}mobile/site.css" />-->
      
{{else}}
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" href="{{$path_css}}960.css" type="text/css" />
    <link rel="stylesheet" href="{{$path_css}}template.css" type="text/css" />
    <link rel="stylesheet" href="{{$path_css}}colour.css" type="text/css" />
{{/if}}
<script src="{{$path_js}}libs/jquery-1.6.2.min.js" type="text/javascript"></script>
{{if $browser eq 'mobile'}}
	<script src="javascript/mobile/master.js"  type="text/javascript"></script>
<!--	<script src="javascript/mobile/jqm-docs.js"></script>
	<script src="javascript/mobile/jquery.mobile-1.0.js"></script>-->
{{/if}}