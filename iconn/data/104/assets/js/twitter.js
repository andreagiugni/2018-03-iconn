$(function(){
    $("#btnAddTweet").on("tap",function () {         
        $("#newTweet").fadeIn(300);
        $("#tweetsList").hide();
        $("#btnAddTweet").hide();
    });
    $("#twitter textarea").keyup(function(){
        if($(this).val()!=""){
            $(".btn-twitter").removeClass("disabled");
        }
        else{
            $(".btn-twitter").addClass("disabled");
        }
    });
    $("#erase-btn").on("tap", function(){
        $("#twitter textarea").val("");
        $(".btn-twitter").addClass("disabled");
    });  
    $("#send-twitter-btn").on("tap",function(){
    	if(!$("#send-twitter-btn").hasClass("disabled")){
	    	sendTweet(
	    		escapeHtml($("#newTweet textarea").val()),
	    		function(myTweet){
	    			var tweet= $(getTweetHtml(myTweet)).hide();
					$("#tweetsList").prepend(tweet);
					$("#newTweet").fadeOut(function(){
                        $("#tweetsList").fadeIn(300);
                        $("#btnAddTweet").fadeIn(300);
						$("#newTweet textarea").val("");						
						$(tweet).slideDown();
	    			});
	    		}
	    	);
    	}
    });
	getTweets(fillTweetList);
});

function sendTweet(tweet,callback) {
    $.ajax({
        method: 'POST',
        url: 'http://'+location.host+'/iconn/phpservices/saveTweet.php',
        dataType: 'json',
        data:{ 
             userId: _USER_ ,
             tweet: tweet
            },
        success: function(data){if(callback)callback(data);},
        error: function(data){}
    });
}

function getTweetHtml(tweet){
	return '<div id="tweet_'+tweet["pkidTweet"]+'" class="tweet animated font-avenir-medium color-blue" >'+ tweet["sbody"] + '</div>';
}

function fillTweetList(tweets){
	var res='';
	if(tweets&&tweets.length>0)
		for (var i = 0; i < tweets.length; i++)
			res+=getTweetHtml(tweets[i]);
	$("#tweetsList").html(res);
}




function getTweets(callback) {
    $.ajax({
        method: 'POST',
        url: 'http://'+location.host+'/iconn/phpservices/getTweets.php',
        dataType: 'json',
        data:{ 
            userId: _USER_, 
            meeting: _MEETING_,
            room: _ROOM_ 
        },
        success: function(data) {
        	if(callback) callback(data);
        }
    });
}

function escapeHtml(string) { 
	var entityMap = {"&": "&amp;","<": "&lt;",">": "&gt;",'"': '&quot;',"'": '&#39;',"/": '&#x2F;'};
	return String(string).replace(/[&<>"'\/]/g, function (s) { return entityMap[s];	});
}



function sqlToJsDate(sqlDate){
    var arr = sqlDate.split(" ");
    var arrData = arr[0].split("-");
    var arrTime = arr[1].split(":");
    return arrData[2]+"/"+(parseInt(arrData[1],10)-1)+"/"+arrData[0]+" "+arrTime[0]+":"+arrTime[1];
}