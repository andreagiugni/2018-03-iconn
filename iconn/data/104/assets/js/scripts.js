function isValidMail(par_email)
{
    mailvalid=false;    
    maildomain = "";
    
    if(par_email.length>0)
    {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

        if(pattern.test(par_email))
        {
            if(maildomain!='')
            {
                if(par_email.indexOf(maildomain)!== -1)
                {
                    mailvalid=true;                
                }
            }
            else
            {
                /*
                se non è stato specificato un dominio la mail è valida
                */
                mailvalid=true; 
            }

        }
    }
    return mailvalid;
}
$(function(){
    $("#refresh-btn").on("tap", function(){
        location.reload();
    });
    $("#btn-agenda").on("tap", function(){
        $("#agenda").fadeIn(300);
    });
    $("#btn-razionale").on("tap", function(){
        $("#razionale").fadeIn(300);
    });
    $("#btn-faculty").on("tap", function(){
        $("#faculty").fadeIn(300);
    });
    $("#btn-qa").on("tap", function(){
        $("#twitter").fadeIn(300);
    });
    $("#btn-informazioni").on("tap", function(){
        $(".accordion-label").removeClass("opened");
        $(".accordion-label[accordion=1]").addClass("opened");
        $(".accordion-text").hide();
        $("#accordion-1").show();
        $("#informazioni").fadeIn(300);
    });
    $(".back-btn").on("tap", function(){
        $(this).closest(".screen-section").fadeOut(300);
        $("#askpresentation").hide();
        $(".btn-presentazioni").removeClass("opened");
    });
    $(".single-tab").on("tap", function(){
        $(".single-tab").removeClass("active");
        $(".img-tab").removeClass("active");
        var imf_ref = $(this).attr("img-reference");
        $(this).addClass("active");
        $("#"+imf_ref).addClass("active");
    });
    $(".accordion-label").on("tap", function(e){
        e.preventDefault();
        var idAccordion = $(this).attr("accordion");
        if(!$(this).hasClass("opened")){
            $(".accordion-label").removeClass("opened");
            $(".accordion-text").not("#accordion-"+idAccordion).delay(1).slideUp(300);
            $(this).addClass("opened");
            $("#accordion-"+idAccordion).slideDown(300);
        }
        else{
            $(this).removeClass("opened");
            $("#accordion-"+idAccordion).slideUp(300);
        } 
    });
    $(".btn-presentazioni").on("tap", function(){
       
        if(!$(this).hasClass("opened")){
            $(this).addClass("opened");
            $("#askpresentation").slideDown(300);
        }
        else
        {
            $(this).removeClass("opened");
            $("#askpresentation").slideUp(300);
        }
    });
    
    $(".change-room").on("tap", function(){
        $(".screen-section").hide();
        var res ="";
        res+='<div class="room color-white bg-yellow text-center font-avenir-heavy" fkIdRoom="181">Sala 1</div>';
        res+='<div class="room color-white bg-blue text-center font-avenir-heavy" fkIdRoom="182">Sala 2</div>';
        $("#choose-room .content-internal .btn-box").html(res);
        $("#choose-room").show();
        $(".room").on("tap", function() {
            logInRoom($(this).attr("fkIdRoom"));
        });
    });
    $("#btnSendRequestPresentation").on("tap",function(){
        if(isValidMail($("#emailtosave").val())){
             $.ajax({
                method: 'POST',
                url: 'http://'+location.host+'/iconn/phpservices/saveUserForPresentationsRequest.php',
                //dataType: 'json',
                cache: false,
                data:{ 
                    device: $_GET["device"],
                    email: $("#emailtosave").val(),
                    meeting : _MEETING_
                    },
                success: function(data){
                    if(data==1)
                    {
                        $("#requestpanel").hide();
                        $("#thanksforrequest").show();
                        $("#thanksforrequest p").text("La richiesta delle presentazioni è stata registrata correttamente!");
                    }
                    else
                    {
                        $("#requestpanel").hide();
                        $("#thanksforrequest").show();
                        $("#thanksforrequest p").text("Richiesta presentazioni già registrata!");
                    }
                    $("html, body").animate({ scrollTop: 0 }, "fast");
                    
//                    alert(JSON.stringify(data));
                    
//                    alert(JSON.stringify(data))
//                    var ws=[];
//                    ws["rooms"]=data;
//                    wsevent('chooseRoom_' + _ROOM_, ws);
                },
                 error: function(data){
                 }
            });
        }
	});
    
});