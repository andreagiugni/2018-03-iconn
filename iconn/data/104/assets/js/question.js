var sliderValue = 50;
function showQuestion(bWsData) {
    //alert(JSON.stringify(bWsData));
    if(localStorage.getItem("lastVotingSession")!=bWsData['vs'])
    {
        localStorage.setItem("lastVotingSession", bWsData['vs']);
        $(".thankyou-box").hide();
        $("#send-answer-btn").addClass("disabled");
    }
    var nCorrectAnswer = 0;
    var q = bWsData['q']['squestion'];
    var nq = bWsData['q']['norder']; 
    
    var questionType = bWsData['q']['bChallenge']; // 1 è domanda tipo duello 0 è domanda caso clinico
    var res = "";
    $(".question-text").text(q);
    $(".question-box-type").hide();
    if(questionType!="1"){
        //domanda caso clinico
        $(".question-caso-clinico").show();
        for (var i = 0; i < bWsData['a'].length; i++){            
            res += "<li pkidquestion='" + bWsData['q']['pkidquestion']+ "' vs='" + bWsData['vs']+ "' pkidanswer='" + bWsData['a'][i]["pkidanswer"] + "'><p class='font-avenir-heavy color-blue'>";
            res += bWsData['a'][i]["sanswer"];
            res += "</p></li>"
        }
        $(".question-box ol").html(res);  
        $(".question-box li").on("tap", function(){
            $(".question-box li").removeClass("choose");
            $(this).addClass("choose");
            $("#send-answer-btn").removeClass("disabled");
            $("html, body").animate({ scrollTop: $(document).height() }, 700);
        });
    }
    else{
        //domanda duello
        $(".question-duello").show();
        $("#send-answer-btn").removeClass("disabled");        
        sliderValue = 50;
        initSlider();
        for (var i = 0; i < bWsData['a'].length; i++){
            var bgClass="bg-grey active", letter="", valueSlider=50;
            
            if(bWsData['a'][i]["sanswer"]=="A"){
                bgClass = "bg-cyano";
                letter = "A";
                valueSlider = 0;
            } 
            if(bWsData['a'][i]["sanswer"]=="B"){
                bgClass = "bg-yellow";
                letter = "B";
                valueSlider = 100;
            }            
            res += "<div class='circle-answer "+bgClass+"' pkidquestion='" + bWsData['q']['pkidquestion']+ "' pkidanswer='" + bWsData['a'][i]["pkidanswer"] + "' vs='" + bWsData['vs']+ "' valueSlider='"+valueSlider+"' >";
            res += "<span class='color-white text-center font-avenir-heavy'>"+letter+"</span>";
            res += "</div>";
        }
        $(".answer-circle-container").html(res);
    }
    $("#send-answer-btn").on("tap",function(){
        confirmAnswer(function(){      
           
            $(".thankyou-box").show();
            $("#send-answer-btn").addClass("disabled");
        }, questionType);
    });
    
}
function initSlider(){
    $( "#slider-answer").slider({
        value: sliderValue,
        min: 0,
        max: 100,
        step: 50,
        slide: function( event, ui ) {
            sliderValue = ui.value;
            $(".circle-answer").removeClass("active");
            $(".circle-answer[valueSlider='"+sliderValue+"']").addClass("active");
        }
    });
}
function showQuestionScreen(bWsData)
{
    var q = bWsData['q']['squestion'];
    $(".screen-question-text").text(q);
}
function confirmAnswer(callback,type) {
    //  alert($_GET["device"]);
    if(type!="1"){
        //domanda di tipo caso clinico
        var result = $(".question-box li.choose").attr("pkidanswer"); 
        $.ajax({
            type: "POST",
            url: "http://" + location.host + "/iconn/phpservices/salvaRispostaUtente.php",
            data: {
                pkidquestion: $(".question-box li").first().attr("pkidquestion"),
                pkidroom: _ROOM_,
                pkidmeeting: _MEETING_,
                pkiddevice: $_GET["device"],
                pkidanswer: result,
                pkidvotehistory: $(".question-box li").first().attr("vs"),
                millisToAnswer: 0,//(30000 - timer.stop())
                openAnswer: ""
            },
            success:callback
        });
    }
    else
    {
        var pkidquestion = $(".circle-answer.active").attr("pkidquestion");
        var pkidvotehistory = $(".circle-answer.active").attr("vs");
        var pkidanswer = $(".circle-answer.active").attr("pkidanswer");
        $.ajax({
            type: "POST",
            url: "http://" + location.host + "/iconn/phpservices/salvaRispostaUtente.php",
            data: {
                pkidquestion: pkidquestion,
                pkidroom: _ROOM_,
                pkidmeeting: _MEETING_,
                pkiddevice: $_GET["device"],
                pkidanswer: pkidanswer,
                pkidvotehistory: pkidvotehistory,
                millisToAnswer: 0,//(30000 - timer.stop())
                openAnswer: ""
            },
            success:callback
        });
    }
    
    
    
        //,success:callback
    
}
function showResults(bWsData) {
    console.log(bWsData);
    //alert(JSON.stringify(bWsData));
    var q = bWsData['q']['squestion'];
    var qType = bWsData['q']['bChallenge'];
    
    var res='';
    if(qType!="1"){
        //risultati caso clinico
        var coeff=800/100;//bWsData['highest_percent']; //390 sono i pixel massimi di larghezza
        $(".screenQuestionStandard .box-question-standard p").text(q);
        res +='<ol id="answerScreenContainer">';
        for (var i = 0; i < bWsData['ans'].length; i++){
                var a = bWsData['ans'][i]["a"];
                var perc = bWsData['ans'][i]['perc'];
                correct=(bWsData['ans'][i]['score']==1?'correct':'');
                res += '<li class="'+correct+'">';
                    res += '<div class="textAnswerScreenContainer" ><p class="font-avenir-heavy color-blue">'+ a +'</p></div>';
                    res += '<div class="barAnswerScreenContainer">';
                        //res += '<div class="bar-external '+correct+'" style="background:#eee; width:'+(coeff*100)+'px"><div class="bar" style="width:'+(coeff*Number(bWsData['ans'][i]['perc']))+'px"></div>';
                        res += '<div class="bar-external '+correct+'" style="background:#eee; width:800px"><div class="bar" style="width:'+(coeff*Number(bWsData['ans'][i]['perc']))+'px"></div>';
                        res += '</div><div class="percentageContainer font-avenir-heavy color-blue">'+perc+'%</div>';
                    //   res += '<div class="stopper"></div>';
                    res += '</div>';
                res += '</li>';
            }
        res += '</ol>';
    }
    else{
        $(".box-question-duello .question-text").text(q);
        //risultati duello
        console.log(bWsData);
        setTimeout(function(){
            var perc_A, perc_B;
            var percGraf = 50;
            for(var i=0;i<bWsData['ans'].length; i++){
                if(bWsData['ans'][i].a == "A"){
                    perc_A = bWsData['ans'][i].perc;
                    if(perc_A!=50){
                        percGraf = - perc_A;
                    }
                }
                if(bWsData['ans'][i].a == "B"){
                    perc_B = bWsData['ans'][i].perc;
                    if(perc_B!=50){
                        percGraf = perc_B;
                    }
                }
            }
            $("#circleA").attr("percentage", perc_A);
            $("#circleB").attr("percentage", perc_B);
            var diff = perc_A - perc_B;
            var rangeDeg = Math.sin(diff/50)*50;
            $(".bar-balance").css("transform", "rotateZ("+(rangeDeg*-1)+"deg)");
            $(".circle").css("transform", "rotateZ("+rangeDeg+"deg)");
        },1500);
    }    
    $("#dynamicContent").html(res);
}