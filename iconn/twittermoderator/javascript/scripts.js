function hideMsg(){$('#error').hide(500);} 

function DeleteTweet(id)
{
    $.ajax({
            type : 'GET',
            url : 'delTweet.php',                            
            data: {
                 idtweet:id
            },
            dataType: "html",
            success : function(data,stato){
                $('#error').removeClass().addClass((data.error === true) ? 'error' : 'success')
                //alert("Data Loaded: " + data);
                $('#error').html(data);
                $('#error').show(500);    
                var loadTimeout = setTimeout(hideMsg, 5000);
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                $('#error').removeClass().addClass('error')
                $('#error').html("Error delete!");
                $('#error').show(500);     

            }
    });
    return false;
};
function ApproveTweet(id)
{
    $.ajax({
            type : 'GET',
            url : 'approveTweet.php',                            
            data: {
                 idtweet:id
            },
            dataType: "html",
            success : function(data,stato){
                $('#error').removeClass().addClass((data.error === true) ? 'error' : 'success')
                //alert("Data Loaded: " + data);
                $('#error').html(data);
                $('#error').show(500);    
                var loadTimeout = setTimeout(hideMsg, 5000);
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                $('#error').removeClass().addClass('error')
                $('#error').html("Error to approve!");
                $('#error').show(500);     

            }
    });
    return false;
};
function restoreTweet(id)
{
    $.ajax({
            type : 'GET',
            url : 'restoreTweet.php',                            
            data: {
                 idtweet:id
            },
            dataType: "html",
            success : function(data,stato){
                $('#error').removeClass().addClass((data.error === true) ? 'error' : 'success')
                //alert("Data Loaded: " + data);
                $('#error').html(data);
                $('#error').show(500);
                var loadTimeout = setTimeout(hideMsg, 5000);    
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                $('#error').removeClass().addClass('error')
                $('#error').html("Error to approve!");
                $('#error').show(500);     

            }
    });
    return false;
};
function removefromscreenTweet(id)
{
    $.ajax({
            type : 'GET',
            url : 'removefromscreeenTweet.php',                            
            data: {
                 idtweet:id
            },
            dataType: "html",
            success : function(data,stato){
                $('#error').removeClass().addClass((data.error === true) ? 'error' : 'success')
                //alert("Data Loaded: " + data);
                $('#error').html(data);
                $('#error').show(500);    
                var loadTimeout = setTimeout(hideMsg, 5000);
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                $('#error').removeClass().addClass('error')
                $('#error').html("Error to approve!");
                $('#error').show(500);     

            }
    });
    return false;
};
function disapproveTweet(id)
{
$.ajax({
        type : 'GET',
        url : 'disapproveTweet.php',                            
        data: {
             idtweet:id
        },
        dataType: "html",
        success : function(data,stato){
            $('#error').removeClass().addClass((data.error === true) ? 'error' : 'success')
            //alert("Data Loaded: " + data);
            $('#error').html(data);
            $('#error').show(500);    
            var loadTimeout = setTimeout(hideMsg, 5000);
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            $('#error').removeClass().addClass('error')
            $('#error').html("Error to approve!");
            $('#error').show(500);     

        }
});
return false;
};
function sendtoscreenTweet(id)
{
$.ajax({
        type : 'GET',
        url : 'sendtoscreenTweet.php',                            
        data: {
             idtweet:id
        },
        dataType: "html",
        success : function(data,stato){
            $('#error').removeClass().addClass((data.error === true) ? 'error' : 'success')
            //alert("Data Loaded: " + data);
            $('#error').html(data);
            $('#error').show(500);    
            var loadTimeout = setTimeout(hideMsg, 5000);
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            $('#error').removeClass().addClass('error')
            $('#error').html("Error to approve!");
            $('#error').show(500);     

        }
});
return false;
};
function editTweet(id)
{
	$('#idtweet').val(id);    
	$.ajax({
			type : 'GET',
			url : 'getTestoTweet.php',                            
			data: {
				 idtweet:id
			},
			dataType: "html",
			success : function(data,stato){                   
				//alert("Data Loaded:" + $.trim(data));
				//var temp=$.trim(data);
				//$('#tweet').html(temp);
				$('#tweet').html($.trim(data));
				$( "#dialog-form" ).dialog( "open" );
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				$('#tweet').html("Error to get message!");
			}
	});
	return false;
}
function saveTweet(id,body)
{
$.ajax({
        type : 'GET',
        url : 'saveTweet.php',                            
        data: {
             idtweet:id, testo:body
        },
        dataType: "html",
        success : function(data,stato){                   
            $('#error').removeClass().addClass((data.error === true) ? 'error' : 'success')
            $('#error').html(data);
            $('#error').show(500);    
            var loadTimeout = setTimeout(hideMsg, 5000);
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
           $('#error').removeClass().addClass('error')
            $('#error').html("Error to save!");
            $('#error').show(500);     
        }
});
return false;
}
function GetToApprove()
{
    var room = $('#room').val();
    var meeting = $('#meeting').val();
    var questionlang =  $('#questionlanguage').val();
    var questiongroup =  $('#questionGroup').val();
     var alert =  $('#alert').val();

    
    $.ajax({
        type : 'GET',
        url : 'getTweets.php',                            
        data: {
            type:'TOAPPROVE',room:room,meeting:meeting,questionlanguage: questionlang, questionGroup:questiongroup, alert:alert
        },
        dataType: "html",
        success : function(data,stato){
            $('#toapprove').html(data);
                 
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            $('#error').removeClass().addClass('error')
            $('#error').html("Error get result!");
            $('#error').show(500);    
             
        }
    });

    return false;
 };
 function GetApproved()
 {
           var room = $('#room').val();
           var meeting = $('#meeting').val();
           var questionlang =  $('#questionlanguage').val();
           var questiongroup =  $('#questionGroup').val();
            var alert =  $('#alert').val();
           
           $.ajax({
               type : 'GET',
               url : 'getTweets.php',                            
               data: {
                   type:'APPROVED',room:room,meeting:meeting,questionlanguage: questionlang, questionGroup:questiongroup, alert:alert
               },
               dataType: "html",
               success : function(data,stato){
                   $('#approved').html(data);
            },
               error : function(XMLHttpRequest, textStatus, errorThrown) {
                     $('#error').removeClass().addClass('error')
                   $('#error').html("Error get result!");
                   $('#error').show(500);     
               }
           });
 
           return false;
 };
 function GetToScreen()
 {
           var room = $('#room').val();
           var meeting = $('#meeting').val();
           var questionlang =  $('#questionlanguage').val();
           var questiongroup =  $('#questionGroup').val();
            var alert =  $('#alert').val();
           
           $.ajax({
               type : 'GET',
               url : 'getTweets.php',                            
               data: {
                   type:'ONSCREEN',room:room,meeting:meeting,questionlanguage: questionlang, questionGroup:questiongroup, alert:alert
               },
               dataType: "html",
               success : function(data,stato){
                   $('#onscreen').html(data);
               },
               error : function(XMLHttpRequest, textStatus, errorThrown) {
                  $('#error').removeClass().addClass('error')
                   $('#error').html("Error get result!");
                   $('#error').show(500);     
               }
           });
 
           return false;
 };
 function GetTrash()
 {
             var room = $('#room').val();
             var meeting = $('#meeting').val();
             var questionlang =  $('#questionlanguage').val();
             var questiongroup =  $('#questionGroup').val();
         
             
             $.ajax({
                 type : 'GET',
                 url : 'getTweets.php',                            
                 data: {
                     type:'TRASH',room:room,meeting:meeting,questionlanguage: questionlang, questionGroup:questiongroup
                 },
                 dataType: "html",
                 success : function(data,stato){
                     $('#trash').html(data);
                 },
                 error : function(XMLHttpRequest, textStatus, errorThrown) {
                     $('#error').removeClass().addClass('error')
                     $('#error').html("Error get result!");
                     $('#error').show(500);     
 
                 }
             });
   
             return false;
 };
function ClearScreen()
{
	var idtweeting = $('#idtweeting').val();
   
    $.ajax({
            type : 'GET',
            url : 'clearScreen.php',                            
            data: {
                idtweeting:idtweeting
            },
            dataType: "html",
            success : function(data,stato){
                $('#error').removeClass().addClass((data.error === true) ? 'error' : 'success')
                //alert("Data Loaded: " + data);
                $('#error').html(data);
                $('#error').show(500);    
                var loadTimeout = setTimeout(hideMsg, 5000);
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                $('#error').removeClass().addClass('error')
                $('#error').html("Error to clear screen!");
                $('#error').show(500);     

            }
    });
    return false;
};