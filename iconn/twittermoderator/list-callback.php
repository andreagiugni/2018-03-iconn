<?php

// CALLBACKS
include("header.php");

if( (isset($_POST['edit']) )
		&& ($currentUserId == $moderatorId) ) 
		{
	
  if(isset($_POST['ntweet'])) {	
	
	//$val = preg_replace('/[\\\]/','',$_POST['tweetbody']);
	//$value = preg_replace('/[\']/','\'\'',$val);
	//$value = utf8_encode($value);
	$value = $_POST['tweetbody'];


	$result = $db->Execute("UPDATE tbltweets SET 
			sbody = '".str_replace("'", "''", $_POST['tweetbody'])."' WHERE pkidTweet = '".$_POST['ntweet']."'");

	
	
			
	if(isset($_POST['approve'])) {
		$date= date("Y-m-d H:i:s");
		$result = $db->Execute("UPDATE tbltweets SET 
			bApproved = '1', dtmApproved = '". $date ."' WHERE pkidTweet = '".$_POST['ntweet']."'");

		$entryData= array( 
				'meeting' => $MEETING,
				'room' =>  $ROOM,
				'service' => 'tweet_'.$ROOM, //*** Importante ***
				'wsData' => array( 
					'command' => 'add',
					'tweetId' => $_POST['ntweet'],
					'dtmApproved'=>$date,
					'sbody'=>$value)
				);
   		 $socket->send(json_encode($entryData));

	}

	else{
		$result = $db->Execute("UPDATE tbltweets 
			SET bApproved = '0' WHERE pkidTweet = '".$_POST['ntweet']."'");

		$entryData= array( 
				'meeting' => $MEETING,
				'room' =>  $ROOM,
				'service' => 'tweet_'.$ROOM, //*** Importante ***
				'wsData' => array( 
					'command' => 'delete',
					'tweetId' => $_POST['ntweet'])
				);
    	$socket->send(json_encode($entryData));	
	}
			
	if(isset($_POST['delete'])) {
		$result = $db->Execute("UPDATE tbltweets SET 
			bDeleted = '1' WHERE pkidTweet = '".$_POST['ntweet']."'");
		$entryData= array( 
				'meeting' => $MEETING,
				'room' =>  $ROOM,
				'service' => 'tweet_'.$ROOM, //*** Importante ***
				'wsData' => array( 
					'command' => 'delete',
					'tweetId' => $_POST['ntweet'])
				);
    	$socket->send(json_encode($entryData));	
	}

	if(isset($_POST['mark'])) {
		$result = $db->Execute("UPDATE tbltweets SET 
			bMarked = '1', dtmMarked = '". date("Y-m-d H:i:s") ."' WHERE pkidTweet = '".$_POST['ntweet']."'");

		$entryData= array( 
					'meeting' => $MEETING,
					'room' =>  $ROOM,
					'service' => 'tweetMarked_'.$ROOM, //*** Importante ***
					'wsData' => array( 
						'command' => 'add',
						'tweetId' => $_POST['ntweet'],
						'sbody'=>$bApproved["sbody"])
					);
	    $socket->send(json_encode($entryData));	


	}
	else{
		$result = $db->Execute("UPDATE tbltweets SET
			 bMarked = '0' WHERE pkidTweet = '".$_POST['ntweet']."'");
		$entryData= array( 
					'meeting' => $MEETING,
					'room' =>  $ROOM,
					'service' => 'tweetMarked_'.$ROOM, //*** Importante ***
					'wsData' => array( 
						'command' => 'delete',
						'tweetId' => $_POST['ntweet'])
					);
	    $socket->send(json_encode($entryData));	
	}
		
	if(isset($_POST['marksingle'])) {
		$result = $db->Execute("UPDATE tbltweets SET 
			bMarked = '0'");
		$result = $db->Execute("UPDATE tbltweets SET 
			bMarked = '1' WHERE pkidTweet = '".$_POST['ntweet']."'");

		$tweet = $db->GetOne("Select sbody from tbltweets where pkidTweet = '".$_POST['ntweet']."'");
		$entryData = array( 
				'meeting' => $MEETING,
				'room' =>  $ROOM,
				'service' => 'tweet_aloneOnScreen_'.$ROOM, //*** Importante ***
				'wsData' => array( 
					"tweet"=>$tweet
					)
				);
		$socket->send(json_encode($entryData));	
			
	}
  }
  

}

// CALLBACK PER UN NUOVO TWEET

if(isset($_POST['new'])||isset($_POST['new'])) {

	if($DISPLAY_DIRECTLY || $YOU_ARE_MODERATOR || ($MODERATOR == 0)) 
		$bApproved = 1;
	else
		$bApproved = 0;
	
	if($DISPLAY_DIRECTLY)
		$bMarked = 1;
	else
		$bMarked = 0;
	

	//$val = preg_replace('/[\\\]/','',$_POST['tweet']);
	//$value = preg_replace('/[\']/','\'\'',$val);
	//$value = preg_replace('/[\']/','\'\'',$_POST['tweet']);
	//$value = preg_replace('/[\']/','\'\'',$_POST['tweet']);
	//$value = str_replace("'", "''", $_POST['tweet']);
	$value = $_POST['tweet'];//utf8_encode($value);
	

	if(!$DISPLAY_DIRECTLY) {

		if($bApproved){
			$date=date("Y-m-d H:i:s");
			$q2 = "INSERT into tbltweets 
					(fkidTweetingSession, fkidSender, fkidDest, sbody, dtmInsert, bApproved, bMarked, dtmApproved)
             VALUES ('".$SESSION."', '".$YOU."', '".$_POST['speaker']."', '".str_replace("'", "''", $_POST['tweet'])."', '". $date .
			 "', '".$bApproved."', '".$bMarked."', '". date("Y-m-d H:i:s") .
			 "')";
			
			$tweetId = $db->Insert_ID();

			$entryData= array( 
				'meeting' => $MEETING,
				'room' =>  $ROOM,
				'service' => 'tweet_'.$ROOM, //*** Importante ***
				'wsData' => array( 
					'command' => 'add',
					'tweetId' => $tweetId,
					'dtmApproved'=>$date,
					'sbody'=>$value)
				);
    		$socket->send(json_encode($entryData));	
		}
		else
		$q2 = "INSERT into tbltweets 
					(fkidTweetingSession, fkidSender, fkidDest, sbody, dtmInsert, bApproved, bMarked)
             VALUES ('".$SESSION."', '".$YOU."', '".$_POST['speaker']."', '".str_replace("'", "''", $_POST['tweet'])."', '". date("Y-m-d H:i:s") .
			 "', '".$bApproved."', '".$bMarked."')";
		
			 
	} else
	
	$q2 = "INSERT into tbltweets 
					(fkidTweetingSession, fkidSender, fkidDest, sbody, dtmInsert, bApproved, bMarked, dtmMarked, dtmApproved)
             VALUES ('".$SESSION."', '".$YOU."', '".$_POST['speaker']."', '".str_replace("'", "''", $_POST['tweet'])."', '". date("Y-m-d H:i:s") .
			 "', '".$bApproved."', '".$bMarked."', '". date("Y-m-d H:i:s") .
			 "', '". date("Y-m-d H:i:s") .
			 "')";


	$res1 = $db->Execute($q2);
	
}
if(isset($_POST['autorefresh']))
		$autorefresh = $_POST['autorefresh'];
	else
		$autorefresh = "";

// INFINE, VAI ALLA LISTA

header("location: "."list.php?you=".$YOU.
  	"&session=".$SESSION.
	"&moderator=".$MODERATOR.
	"&title=".$TITLE.
	"&meeting=".$MEETING.
	"&lang=".$LANG.
	"&room=".$ROOM.
	"&device=".$DEVICE.
	"&autorefresh=".$autorefresh.
	"&expscreen=".$_POST['expscreen'].
	"&expapproved=".$_POST['expapproved'].
	"&exptoapprove=".$_POST['exptoapprove'].
	"&you_are_speaker=".$YOU_ARE_SPEAKER.")");
	

/*	$entryData= array( 
				'meeting' => $MEETING,
				'room' =>  $ROOM,
				'service' => 'flush_'.$ROOM //*** Importante ***
				);
    		$socket->send(json_encode($entryData));		*/

//<input type='hidden' name='autorefresh' value='".$autorefresh."'>
	
?>