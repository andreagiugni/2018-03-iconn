<?php 
include("connect.php");

function translatecontent($id, $table, $lang) {
	
	global $db;
	
	$row = NULL;
	
if($table == "questions") {

	$q22 =  "SELECT * FROM tbltranslations WHERE fkid  = '".$id."' AND stable = '".$table."' AND slanguage = '".$lang."'";
	//echo $q22;
	
	$row = $db->GetRow($q22);	
}

if($table == "answers") {

	$q33 =  "SELECT * FROM tbltranslations WHERE fkid  = '".$id."' AND stable = '".$table."' AND slanguage = '".$lang."'";
	
	//echo $q33;
	
	$row = $db->GetRow($q33);	
}

	if($row) 
		return $row['stranslation'];
	else
		return NULL;
}


function strictify ( $string ) {

	//return htmlentities($string);
	
	$fixed = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
/*
       $fixed = htmlspecialchars( $string, ENT_QUOTES );
		
       $trans_array = array();
       for ($i=127; $i<255; $i++) {
           $trans_array[chr($i)] = "&#" . $i . ";";
       }

       $really_fixed = strtr($fixed, $trans_array);

       return $really_fixed;
*/
		return $fixed;
		
   }
   
   
$out = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';


	
	$q22 = "SELECT * FROM tblstack WHERE 
					( (scommand = 'LOGIN') AND
					 ((fkidmeeting = '".$_GET['meeting']."') OR (fkidmeeting = 0)) AND 
					 ((fkidroom = '".$_GET['room']."') OR (fkidroom = 0)) AND
					 ((fkiddevice = '".$_GET['device']."') OR (fkiddevice = 0)) AND
					 (bpersistent = '1')) ORDER BY pkidstack DESC
					
					";

	//echo $q22;
	
		

	
	$rowx = $db->GetRow($q22);

	$ROOM = $_GET['room'];
	
	if($rowx) {

	
	$out .= '	<dict>
		<key>Command</key>
		<string>'.$rowx['scommand'].'</string>
		<key>Argument</key>
		<string><![CDATA['.$rowx['sargument'].']]></string>
		<key>Params</key>
		<string><![CDATA['.$rowx['sparams'].']]></string>
		<key>Date</key>
		<string><![CDATA['.$rowx['dtminsert'].']]></string>
		<key>Room</key>
		<string>'.$rowx['fkidroom'].'</string>
		<key>Meeting</key>
		<string>'.$rowx['fkidmeeting'].'</string>
		<key>KeepOn</key>
		<string>'.$rowx['bkeepon'].'</string>
		<key>Block</key>
		<string>'.$rowx['bblock'].'</string>
	</dict>';		
	  
	  
	   
		$ROOM = $rowx['sargument'];
	
	//echo "ROOM ".$ROOM;
	}		
	
		$q2 = "SELECT * FROM tblstack WHERE 
					(
					 ((fkidmeeting = '".$_GET['meeting']."') OR (fkidmeeting = 0)) AND  (scommand NOT LIKE 'LOGIN') AND 
					 ((fkidroom = '".$ROOM."') OR (fkidroom = 0)) AND
					 ((fkiddevice = '".$_GET['device']."') OR (fkiddevice = 0)) AND
					 (bpersistent = '1')) ORDER BY pkidstack DESC
					
					";

		//echo $q2;
	
		$rows = $db->GetAll($q2);
		
					/*
$rows = $db->GetAll("SELECT * FROM tblstack WHERE 
					(fkidroom = '".$_GET['room']."') AND
					((fkiddevice = '".$_GET['device']."') OR (fkiddevice = '0')) AND
					(bpersistent = '1')");	
	
*/

function ProcessRows($rows, $nologin) {

global $out;
global $db;

	//echo "ENTERING PROCESSROWS CON NOLOGIN = ".$nologin;

	foreach($rows as $row) {

	/*
	if(($row['scommand'] == "LOGIN") && ($nologin == 0)) {
	
		$q99 = "SELECT * FROM tblstack WHERE 
					(
					 ((fkidmeeting = '".$_GET['meeting']."') OR (fkidmeeting = 0)) AND 
					 ((fkidroom = '".$row['sargument']."') ) AND
					 ((fkiddevice = '".$_GET['device']."') OR (fkiddevice = 0)) AND
					 (bpersistent = '1')) ORDER BY pkidstack ASC
					
					";
					
		//		echo $q99;

		$rows99 = $db->GetAll($q99);
		
		$out .= ProcessRows($rows99, 1);
		break;
	}
	*/
	
if(($row['scommand'] == "ASK") || ($row['scommand'] == "GRADING")) {
	
	if($row['sargument'] != "") {
		
		$row['scommand'] = "URL";
		$row['sargument'] = "data/".$_GET['meeting']."/".$row['sargument'];
		
		$q = 0;
		$count = 0;
	
		if(isset($row['sparams']))
			parse_str($row['sparams']);
		
	
		
		$rw = $db->GetRow("SELECT * FROM tblquestionevaluationentity WHERE pkidquestion = '".$q."'");
		
		$qws = $db->GetAll("SELECT * FROM tblanswerevaluation WHERE ngroup = '".$rw['ngroupanswer']."' ORDER BY norder ASC");
		
		$par = "enableAnswers=1&idQuestion=".$q."&idVoteHistory=".$count."&numMaxAnswer=".$rw['nmaxanswers']."&arrayRisposte=";
			
		$answersPar=""; 
			foreach($qws as $qw) {
				if($answersPar=="")
					$answersPar .= $qw['pkidanswer'];
				else
					$answersPar .= "|".$qw['pkidanswer'];
			}

		$par.=$answersPar;
		$row['sparams'] = $par;
		
	}
else {	
	
	if(isset($_GET['lang']))
		$QUESTIONLANGUAGE = $_GET['lang'];
	else
		$QUESTIONLANGUAGE = "";
		
		$q = 0;
		$count = 0;
		
		//echo $row['sparams'];
		
		if(isset($row['sparams']))
			parse_str($row['sparams']);
		
		//echo $q;
		//echo $count;

		$rw = $db->GetRow("SELECT * FROM tblquestionevaluationentity WHERE pkidquestion = '".$q."'");
	
	$translation = translatecontent($q, "questions", $QUESTIONLANGUAGE);

		if($translation)  
			$rw['squestion'] = $translation;
			
	//  if(!isset($rw['squestion'])) {
	//		$out_domanda = "###1###0###Errore###DOMANDA NON TROVATA"."###";
	//		//$row['sparams'] = "q=0&count=0";
	//  }
	//  else 
	//  {
		
		$testogruppo = "";
	
		if(isset($rw['squestion']) && $rw['squestion'] != "") 
			$out_domanda = "###".$rw['nmaxanswers']."###".$rw['norder']."###".$testogruppo."###".strictify($rw['squestion'])."###";
		else {
			$out_domanda = "###1###0###Errore###DOMANDA NON TROVATA"."###";
			$row['sparams'] = "q=0&count=0";
			continue;
		}

		if(isset($rw['ngroupanswer'])) {
			$qws = $db->GetAll("SELECT * FROM tblanswerevaluation WHERE ngroup = '".$rw['ngroupanswer']."'");
		
			foreach($qws as $qw) {
			
			
			
			$translation = translatecontent($qw['pkidanswer'], "answers", $QUESTIONLANGUAGE);

				if($translation)  
					$qw['sanswer'] = $translation;
					
		 		$out_domanda .= $qw['pkidanswer']."###".strictify($qw['sanswer'])."###";
			}
		}
		
		
	 // }
		$row['sargument'] = $out_domanda;
}
}

$out .= '	<dict>
		<key>Command</key>
		<string>'.$row['scommand'].'</string>
		<key>Argument</key>
		<string><![CDATA['.$row['sargument'].']]></string>
		<key>Params</key>
		<string><![CDATA['.$row['sparams'].']]></string>
		<key>Date</key>
		<string><![CDATA['.$row['dtminsert'].']]></string>
		<key>Room</key>
		<string>'.$row['fkidroom'].'</string>
		<key>Meeting</key>
		<string>'.$row['fkidmeeting'].'</string>
		<key>KeepOn</key>
		<string>'.$row['bkeepon'].'</string>
		<key>Block</key>
		<string>'.$row['bblock'].'</string>
	</dict>';			


break;	
}	


}

ProcessRows($rows, 0);

$out .= '</array>
</plist>';

echo $out;

?>